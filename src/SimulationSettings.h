#pragma once
#include <string>
#include <vector>
#include <map>
#include <sstream>
#include <iomanip>
#include <algorithm>

class SimulationSettings
{
public:
	
	enum class Neighborhood { VonNeumann, Moore, VonNeumannMoore };
	enum class NeighborChoice { Random = 0, Clockwise = -1, Smartest = 1 };
	enum class CompetenceChoice { Random, LeftRight, RightLeft };
	enum class StatisticsElement { fk, nk, nK, tau, anyChunk, nKvsLp, end };
	enum class RangeVariable {L, k, p, tFrom, tTo};

	int threads = 1;
	int simulations = 100;
	int L;
	int competences;
	double p;
	int teachingRangeFrom;
	int teachingRangeTo;
	bool teachingRangeToMax;
	Neighborhood neighborhood;
	NeighborChoice neighborChoice;
	CompetenceChoice competenceChoice;
	
	std::string inputFile = "input.dat";
	std::string outputFile = "results.txt";
	std::string gnuplotFile = "gnuplot.plt";
    bool generateGnuplot = false;
    bool generateEPS = false;

	std::vector<StatisticsElement> statistics;
	
	static std::map<std::string, StatisticsElement> sStatisticsMap;

    static std::map<SimulationSettings::RangeVariable, std::string> sVariablesMap;
    static std::map<SimulationSettings::StatisticsElement, SimulationSettings::RangeVariable> sRangedMap;

	std::map<RangeVariable, std::vector<double>> ranges;


    std::string toString(const std::string& excl = "") const
    {
		std::stringstream stream;
		int perc = 1;
        stream << std::fixed << std::setprecision(3) << p;
		if (stream.str()[3] != '0')
			perc = 2;
        stream.str("");
        stream << 'p' << std::fixed << std::setprecision(perc) << p;
        auto sP = stream.str();
        sP.erase(sP.find_first_of('.'), 1);
        if (excl == "p")
            sP = "";
        stream.str("");
        auto sL = (excl == "L" ? "" : "L" + std::to_string(L));
        auto sK = (excl == "K" ? "" : "K" + std::to_string(competences));
        auto sT = (excl == "t" ? "" : "t(" + std::to_string(teachingRangeFrom) + "_" + std::to_string(teachingRangeTo) + ")");
        stream << sL << sK << sP;
        if (ranges.count(RangeVariable::tFrom) > 0 || ranges.count(RangeVariable::tTo) > 0)
            stream << sT;

        return stream.str();
    }
};

