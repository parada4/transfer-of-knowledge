#include "SimulationSettings.h"

std::map<std::string, SimulationSettings::StatisticsElement> SimulationSettings::sStatisticsMap =
        { { "f(k)", SimulationSettings::StatisticsElement::fk },
          { "n(k)", SimulationSettings::StatisticsElement::nk } ,
          { "n(K)", SimulationSettings::StatisticsElement::nK } ,
          { "tau", SimulationSettings::StatisticsElement::tau } ,
          { "f", SimulationSettings::StatisticsElement::anyChunk },
          { "n(K)vsLp", SimulationSettings::StatisticsElement::nKvsLp } };

std::map<SimulationSettings::RangeVariable, std::string> SimulationSettings::sVariablesMap =
        { {SimulationSettings::RangeVariable::L, "l"},
          {SimulationSettings::RangeVariable::k, "k"},
          {SimulationSettings::RangeVariable::p, "p"},
          {SimulationSettings::RangeVariable::tFrom, "t"},
          {SimulationSettings::RangeVariable::tTo, "t"} };

std::map<SimulationSettings::StatisticsElement, SimulationSettings::RangeVariable> SimulationSettings::sRangedMap =
        { {SimulationSettings::StatisticsElement::nK, SimulationSettings::RangeVariable::p},
         {SimulationSettings::StatisticsElement::tau, SimulationSettings::RangeVariable::p},
          {SimulationSettings::StatisticsElement::anyChunk, SimulationSettings::RangeVariable::p} ,
        {SimulationSettings::StatisticsElement::nKvsLp, SimulationSettings::RangeVariable::p} };
