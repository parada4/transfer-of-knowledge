#include "Simulation.h"
#include "Statistics.h"

const int maxIterations = 500;

std::vector<std::vector<int>> neighborsOffsets;

Simulation::Simulation(SimulationSettings* s) : mSettings(s), mSize(s->L*s->L),
	mCurrent((unsigned long) mSize),
	mPrevious((unsigned long) mSize),
	mKChunks((unsigned long) mSettings->competences),
	mSumChunks((unsigned long) (mSettings->competences + 1))
{
	neighborsOffsets =
	{
		{ -1, -s->L, 1, s->L },
		{ -1, -s->L - 1, -s->L, -s->L + 1, 1, s->L + 1, s->L, s->L - 1 },
		{ -2, -1, -s->L - 1, -2 * s->L, -s->L, -s->L + 1, 1, 2, s->L + 1, s->L, 2 * s->L, s->L - 1 }
	};

	initializeVector(mCurrent);
	initializeVector(mPrevious);
	copyVectors();
	initializeNeighbors(mCurrent);
	initializeNeighbors(mPrevious);
	
	for (bool& b : mStatistics)
		b = false;

	for (SimulationSettings::StatisticsElement&ss : s->statistics)
	{
		if ((ss == SimulationSettings::StatisticsElement::nK || ss == SimulationSettings::StatisticsElement::nKvsLp) && mSettings->ranges[SimulationSettings::RangeVariable::p].size() == 0)
			continue;

		mStatistics[int(ss)] = true;
	}
	
	mCurrentP = (int)std::distance(s->ranges[SimulationSettings::RangeVariable::p].begin(), std::find(s->ranges[SimulationSettings::RangeVariable::p].begin(), s->ranges[SimulationSettings::RangeVariable::p].end(), s->p));
}

Simulation::~Simulation()
{
}

void Simulation::run()
{
	Statistics::sInstance().startSeries();
	Statistics::sInstance().startSingleSeries(mSettings);

	updateStatistics(0);

	for (int i = 1; i < maxIterations; ++i)
	{
        for (auto&c : mCurrent)
            c.update();
        
		copyVectors();
		updateStatistics(i);

		if (mCurrentSum == mPrevSum)
		{
			Statistics::sInstance().finishSeries();
			Statistics::sInstance().finishSingleSeries();
            endSingleStatistics(i);
			break;
		}
	}
} 

void Simulation::updateStatistics(int t)
{
	mPrevSum = mCurrentSum;
	mCurrentSum = 0;
	int fulls = 0;
	if (doStatistics(SimulationSettings::StatisticsElement::fk))
		std::fill(mKChunks.begin(), mKChunks.end(), 0);
	if (doStatistics(SimulationSettings::StatisticsElement::nk))
		std::fill(mSumChunks.begin(), mSumChunks.end(), 0);

    for (auto&c : mCurrent)
	{
		auto sum = c.sum();
		
		if (doStatistics(SimulationSettings::StatisticsElement::nK) || doStatistics(SimulationSettings::StatisticsElement::nKvsLp))
			fulls += int(sum == mSettings->competences);
		
		mCurrentSum += sum;
		
		if (doStatistics(SimulationSettings::StatisticsElement::nk))
			++mSumChunks[sum];

		if (doStatistics(SimulationSettings::StatisticsElement::fk))
			for (int j = 0; j < mSettings->competences; ++j)
				mKChunks[j] += c[j];
	}

    mLastFulls = fulls;

	if (mPrevSum == mCurrentSum)
		return;

	for (int i = 0; i < mSettings->competences + 1; ++i)
	{
		if (i < mSettings->competences && doStatistics(SimulationSettings::StatisticsElement::fk))
			Statistics::sInstance().addChunk(SimulationSettings::StatisticsElement::fk, i, t, double(mKChunks[i]) / mSize);

		if (doStatistics(SimulationSettings::StatisticsElement::nk))
			Statistics::sInstance().addChunk(SimulationSettings::StatisticsElement::nk, i, t, double(mSumChunks[i]) / mSize);
	}

	if (doStatistics(SimulationSettings::StatisticsElement::nK))
		Statistics::sInstance().addSingleChunk(mSettings, SimulationSettings::StatisticsElement::nK, SimulationSettings::RangeVariable::p, mCurrentP, t, fulls / double(mSize));
}

void Simulation::reset()
{
    for (auto& c : mCurrent)
        c.reset();
    
	copyVectors();
}

void Simulation::endSingleStatistics(const int& t)
{
    if (doStatistics(SimulationSettings::StatisticsElement::nKvsLp))
    {
        Statistics::sInstance().addSingleChunk(mSettings, SimulationSettings::StatisticsElement::nKvsLp, SimulationSettings::RangeVariable::p, mCurrentP, t, mLastFulls / double(mSize));
    }
    if (doStatistics(SimulationSettings::StatisticsElement::tau))
    {
        Statistics::sInstance().addSingleChunk(mSettings, SimulationSettings::StatisticsElement::tau, SimulationSettings::RangeVariable::p, mCurrentP, t, t);
    }
	if (doStatistics(SimulationSettings::StatisticsElement::anyChunk))
	{
        double val = 0.0;
        for (auto& c : mCurrent)
            for (int j = 0; j < mSettings->competences; ++j)
                val += c[j];

        val /= mSettings->competences * mSize;

        Statistics::sInstance().addSingleChunk(mSettings, SimulationSettings::StatisticsElement::anyChunk, SimulationSettings::RangeVariable::p, mCurrentP, t, val * 100);
	}
}

void Simulation::initializeVector(std::vector<Cell>& vec)
{
	int id = 0;
	for (auto&c : vec)
		c.init(mSettings, id++);
}

void Simulation::initializeNeighbors(std::vector<Cell>& vec)
{
	auto& other = &vec == &mCurrent ? mPrevious : mCurrent;
	auto& offsets = neighborsOffsets[int(mSettings->neighborhood)];

	for (auto& v : vec)
	{
		for (int j = 0; j < offsets.size(); ++j)
		{
			auto nId = getNeighborId(v.id(), offsets[j]);
			v.setNeighbor(other[nId], j);
		}
	}
}

int Simulation::getNeighborId(const int& index, const int& offset)
{
	auto nIndex = index + offset;
	if (nIndex < 0)
		nIndex += mSize;

	return nIndex % mSize;
}

void Simulation::copyVectors()
{
	for (int i = 0; i < mSize; ++i)
		mPrevious[i] = mCurrent[i];
}
