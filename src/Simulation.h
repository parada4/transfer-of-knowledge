#pragma once

#include "SimulationSettings.h"
#include "Cell.h"
#include <memory>
#include <thread>
#include <array>

class Simulation
{
public:
	explicit Simulation(SimulationSettings* s);
	~Simulation();

	void run();
	void updateStatistics(int t);
	void reset();

	void endSingleStatistics(const int& t);

private:
	void initializeVector(std::vector<Cell>& vec);
	void initializeNeighbors(std::vector<Cell>& vec);
	int getNeighborId(const int& index, const int& offset);

	void copyVectors();
	bool doStatistics(SimulationSettings::StatisticsElement e) { return mStatistics[int(e)]; }

	SimulationSettings* mSettings;
	int mSize;
	std::vector<Cell> mCurrent;
	std::vector<Cell> mPrevious;

	std::vector<int> mKChunks;
	std::vector<int> mSumChunks;
	int mCurrentP;

	std::array<bool, int(SimulationSettings::StatisticsElement::end)> mStatistics;

	int mCurrentSum = 0;
	int mPrevSum = 0;

	int mLastFulls = 0;
};

