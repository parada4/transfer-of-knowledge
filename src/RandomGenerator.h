#pragma once

namespace RandomGenerator
{
	double randomDouble(const double& from = 0.0, const double& to = 1.0);
	int randomInt(const int& from, int to);
}

