#include "Statistics.h"
#include "SimulationSettings.h"
#include "StringHelper.h"
#include <algorithm>
#include <fstream>
#include <iostream>
#include <iomanip>

std::map<SimulationSettings::StatisticsElement, std::pair<std::function<StatisticsOperation*(SimulationSettings*)>, std::function<void(StatisticsOperation*,SimulationSettings*)>>> Statistics::sOperations =
{
	{ 
		SimulationSettings::StatisticsElement::fk,
		std::make_pair
		(
			[](SimulationSettings*ss) {return new Statistics_fk(ss->competences); },
			[](StatisticsOperation*so, SimulationSettings*ss) { so->init(ss,ss->competences); }
		)
	},
	{
		SimulationSettings::StatisticsElement::nk,
		std::make_pair
		(
			[](SimulationSettings*ss) {return new Statistics_nk(ss->competences + 1); },
			[](StatisticsOperation*so, SimulationSettings*ss) { so->init(ss,ss->competences + 1); }
		)
	},
    {
        SimulationSettings::StatisticsElement::nK,
        std::make_pair
                (
                        [](SimulationSettings*ss) {return new Statistics_nK(ss->ranges[SimulationSettings::RangeVariable::p]); },
                        [](StatisticsOperation*so, SimulationSettings*ss) { dynamic_cast<StatisticsSingle*>(so)->init(ss, ss->ranges[SimulationSettings::RangeVariable::p]); }
                )
    },
    {
            SimulationSettings::StatisticsElement::tau,
            std::make_pair
                    (
                            [](SimulationSettings*ss) {return new Statistics_Tau(ss->ranges[SimulationSettings::RangeVariable::p]); },
                            [](StatisticsOperation*so, SimulationSettings*ss) { dynamic_cast<StatisticsSingle*>(so)->init(ss, ss->ranges[SimulationSettings::RangeVariable::p]); }
                    )
    },
	{
			SimulationSettings::StatisticsElement::anyChunk,
			std::make_pair
					(
							[](SimulationSettings*ss) {return new Statistics_AnyChunk(ss->ranges[SimulationSettings::RangeVariable::p]); },
							[](StatisticsOperation*so, SimulationSettings*ss) { dynamic_cast<StatisticsSingle*>(so)->init(ss, ss->ranges[SimulationSettings::RangeVariable::p]); }
					)
	},
	{
			SimulationSettings::StatisticsElement::nKvsLp,
			std::make_pair
					(
							[](SimulationSettings*ss) {return new Statistics_nKvsLp(ss->ranges[SimulationSettings::RangeVariable::p]); },
							[](StatisticsOperation*so, SimulationSettings*ss) { dynamic_cast<Statistics_nKvsLp*>(so)->init(ss, (int) ss->ranges[SimulationSettings::RangeVariable::p].size()); }
					)
	}
};

Statistics::~Statistics()
{
	for (std::pair<SimulationSettings::StatisticsElement, StatisticsOperation*>&o : mOperations)
		delete o.second;
}

void Statistics::init(SimulationSettings* settings)
{
	bool add = mOperations.size() == 0;
	for (SimulationSettings::StatisticsElement&s : settings->statistics)
	{
		if (add)
		{
			mMapIndexes[s] = mOperations.size();
			auto el = sOperations[s].first(settings);
			if (el->singleFile())
			{
				delete el;
				continue;
			}
			
			mOperations.push_back(std::make_pair(s, el));
			mGlobalOperations.push_back(mOperations[mMapIndexes[s]]);
			
			sOperations[s].second(mOperations[mMapIndexes[s]].second, settings);
		}
		else if (s != SimulationSettings::StatisticsElement::nK &&
                 s != SimulationSettings::StatisticsElement::tau &&
			 	 s != SimulationSettings::StatisticsElement::anyChunk &&
			     s != SimulationSettings::StatisticsElement::nKvsLp)
		sOperations[s].second(mOperations[mMapIndexes[s]].second, settings);

	}
}

void Statistics::startSeries()
{
	for (std::pair<SimulationSettings::StatisticsElement, StatisticsOperation*>& o : mGlobalOperations)
		o.second->startSeries();
}

void Statistics::finishSeries()
{
	for (std::pair<SimulationSettings::StatisticsElement, StatisticsOperation*>& o : mGlobalOperations)
		o.second->finishSeries();
}

void Statistics::normalize()
{
	for (std::pair<SimulationSettings::StatisticsElement, StatisticsOperation*>& o : mGlobalOperations)
		o.second->normalize();
}

void Statistics::startSingleSeries(SimulationSettings* settings)
{
    for (auto& op : {SimulationSettings::StatisticsElement::nK, SimulationSettings::StatisticsElement::tau, SimulationSettings::StatisticsElement::anyChunk, SimulationSettings::StatisticsElement::nKvsLp})
    {
        if (hasOperation(op) && settings->ranges[SimulationSettings::RangeVariable::p].size() > 0)
        {
            auto key = toKey(settings, "p");
            if (mSingles[op].count(key) == 0)
            {
                auto ss = dynamic_cast<StatisticsSingle*>(sOperations[op].first(settings));
                mSingles[op][key] = ss;
                mSignleOperations.push_back(std::make_pair(op, ss));
                sOperations[op].second(ss, settings);
            }
            else
                mSingles[op][key]->startSeries();
        }
    }
}

void Statistics::finishSingleSeries()
{
	for (std::pair<SimulationSettings::StatisticsElement, StatisticsSingle*>& o : mSignleOperations)
    {
        if (o.first == SimulationSettings::StatisticsElement::nK)
		    o.second->finishSeries();
    }
}

void Statistics::normalizeSingle()
{
	for (std::pair<SimulationSettings::StatisticsElement, StatisticsSingle*>& o : mSignleOperations)
		o.second->normalize();
}

void Statistics::addChunk(SimulationSettings::StatisticsElement e, int K, int t, double val)
{
	mOperations[mMapIndexes[e]].second->addChunk(K, t, val);
}

void Statistics::addSingleChunk(SimulationSettings * s, SimulationSettings::StatisticsElement e,SimulationSettings::RangeVariable r, int i, int t, double val)
{
	mSingles[e][toKey(s, SimulationSettings::sVariablesMap[r])]->addChunk(i, t, val);
}

Statistics& Statistics::sInstance()
{
	static Statistics statistics;
	return statistics;
}
bool Statistics::lastOperation(SimulationSettings::StatisticsElement e) 
{ 
	bool single = std::count_if(mSignleOperations.begin(), mSignleOperations.end(), [&e](std::pair<SimulationSettings::StatisticsElement, StatisticsSingle*>&p) {return p.first == e; }) > 0;
	return single || mMapIndexes[e] == mGlobalOperations.size() - 1; 
}

void Statistics::dumpGlobalResults(const std::string& outputFile, SimulationSettings* settings)
{
	dumpResults(outputFile, settings, mGlobalOperations);
}

void Statistics::dumpResults(const std::string& outputFile, SimulationSettings* settings, std::vector<std::pair<SimulationSettings::StatisticsElement, StatisticsOperation*>>& vec, const std::string& excl)
{
    std::vector<std::string> excls = {excl};
    dumpResults(outputFile, settings, vec, excls);
}
void Statistics::dumpResults(const std::string& outputFile, SimulationSettings* settings, std::vector<std::pair<SimulationSettings::StatisticsElement, StatisticsOperation*>>& vec, const std::vector<std::string>& excl)
{
	if (vec.size() == 0)
		return;

	std::ifstream input(settings->inputFile, std::ios::in);
	std::ofstream file(outputFile, std::ios::out);
	if (!file)
	{
		std::cout << "Can't open result file to write.\n";
		return;
	}

	std::string line;
	while (getline(input, line))
	{
		if (line.find("Autogenerated") != line.npos && line.find("example") != line.npos)
			continue;

		auto start = line.find("[");
		auto end = line.find("]");
		auto hash = line.find_first_of('#');
		if ((start != line.npos && end != line.npos && hash == line.npos || start < hash) && !containsExclude(line, excl))
		{
			std::string val;
			if (line.find("#L") != line.npos)
				val = std::to_string(settings->L);
			else if (line.find("#c") != line.npos)
				val = std::to_string(settings->competences);
			else if (line.find("#p") != line.npos)
				val = std::to_string(settings->p);
			
			if (val.find(".") != val.npos)
			{
				auto last0 = val.find_last_not_of('0');
				if (last0 != val.npos)
					val.erase(last0+1, val.size() - last0 + 1);
			}
			if (hash != line.npos)
				end = hash;
			else
				end += 1;

			line.replace(start, end, val+"\t\t");
		}

		file << '#' << line << '\n';
	}

    auto first = vec.front().second;

    file << "#S=" << settings->simulations << "\n";

    for (std::pair<SimulationSettings::StatisticsElement, StatisticsOperation*>&o : vec)
        file << o.second->preHeaders(lastOperation(o.first));

    file << "#";
    file << first->firstColumn();
    file << "\t";

    for (std::pair<SimulationSettings::StatisticsElement, StatisticsOperation*>&o : vec)
		file << o.second->headers(lastOperation(o.first));

	mMaxT = vec.front().second->maxT();

	for (int t = 0; t < mMaxT; ++t)
	{
		file << first->firstColumnItem(t) << '\t';

		for (std::pair<SimulationSettings::StatisticsElement, StatisticsOperation*>&o : vec)
			file << o.second->results(t, lastOperation(o.first));
	}
}
void Statistics::dumpGlobalGnuplot(const std::string& gnuplotFile, SimulationSettings* settings)
{
	dumpGnuplot(gnuplotFile, settings, mGlobalOperations);
}

void Statistics::dumpGnuplot(const std::string& gnuplotFile, SimulationSettings* settings, std::vector<std::pair<SimulationSettings::StatisticsElement, StatisticsOperation*>>& vec, const std::string& excl)
{
	if (vec.size() == 0)
		return;

	std::ofstream file(gnuplotFile, std::ios::out);
	if (!file)
	{
		std::cout << "Can't open gnuplot script file to write.\n";
		return;
	}

	std::string font = "Times-Roman";
	auto size = vec.size();

	std::string title;
    if (excl == "Kp")
        title += "L = " + std::to_string(settings->L);
    else
    {
        if (excl != "L")
            title += "L = " + std::to_string(settings->L);
    	if (excl != "K")
    	{
    		if (title != "")
			title += ", K = " + std::to_string(settings->competences);
		    else
		    	title += "K = " + std::to_string(settings->competences);
	    }
	    if (excl != "p")
	    {
		    std::stringstream ss; ss << std::fixed << std::setprecision(1) << settings->p;
		    if (title != "")
			    title += ", p = " + ss.str();
		    else
		    	title += "p = " + ss.str();
	    }
    }

	mMaxT = vec.front().second->maxT();

	if (settings->generateEPS)
   {
		file << "set terminal postscript eps size 14,10.5 enhanced color font '" << font << ",95'\n";
      file << "set tics font '" << font << ", 70'\n";
   }
	else
		file << "set term png\n";

	file << "set xrange[0:" << mMaxT-1 << "]\n"
    	 << "set title '{/Times-Italic " << title << "}' offset 0, -0.5 \n"
		 << "set xlabel '{/Times-Italic t}' offset 0,1\n"
		 // << "set key font '" << font << ",20'\n"
		 << "set key outside\n"
		 << "set key font 'Times-Roman, 60'\n"
		 << "set key samplen 3 spacing .9\n"
		 << "set colorsequence classic\n";

	int from = 2;

	for (std::pair<SimulationSettings::StatisticsElement, StatisticsOperation*>& o : vec)
	{
		auto extenstion = settings->generateEPS ? "eps" : "png";
        auto prefix = o.second->StatisticsOperation::title();

        file << "set out '" << StringHelper::split(gnuplotFile, ".p").front();
        if (!StringHelper::contains(gnuplotFile, prefix))
            file << "_" << prefix;

        file << "." << extenstion << "'\n";


		if (o.second->isPercentage())
    		file << "set yrange[0:100]\n";
    	else
    		file << "set yrange[0:1]\n";
	
        file << o.second->gnuplot(from, settings->outputFile);
		from += o.second->size();
	}
}

void Statistics::dumpSingleResults(SimulationSettings* settings)
{
	for (std::map<SimulationSettings::StatisticsElement , std::map<std::string, StatisticsSingle*>>::value_type& s : mSingles)
	{
		dumpSingleFileResults(settings, SimulationSettings::sRangedMap[s.first], s.second);
	}
}

void Statistics::dumpSingleFileResults(SimulationSettings* settings, SimulationSettings::RangeVariable r, std::map<std::string, StatisticsSingle*>& map)
{
    auto initFile = settings->outputFile;
	auto outputFile = settings->outputFile;
	for (std::map<std::string, StatisticsSingle*>::value_type&p : map)
	{
        std::vector<std::string> excls;
        std::string gnuplotExcl;
        settings->outputFile = outputFile = initFile;
		auto spl = StringHelper::split(outputFile, "_");
		auto dot = outputFile.find_last_of('.');
        std::string prefix, ext;
        if (dot != outputFile.npos)
        {
            ext = outputFile.substr(dot, outputFile.size() - dot);
            prefix =  outputFile.substr(0, dot);
        }
        else
        {
            ext = ".txt";
            prefix = outputFile;
        }

        if (p.second->type() == SimulationSettings::StatisticsElement::tau || p.second->type() == SimulationSettings::StatisticsElement::anyChunk)
        {
            std::string L = p.first;
            L.erase(0, 1);
            settings->L = atoi(L.c_str());

            outputFile = prefix + p.first + "_" + p.second->title();

            excls.push_back("#p");
            excls.push_back("#comp");

            gnuplotExcl = "Kp";
        }
        else if (r == SimulationSettings::RangeVariable::p)
		{
            auto ls = StringHelper::split(p.first, "K");
            auto l = ls.front().substr(1, ls.front().size()-1);
            auto ks = StringHelper::split(ls.back(), "t");
            auto k = ks.size() > 0 ? ks.front() : ls.back();
            settings->L = atoi(l.c_str());
			settings->competences = atoi(k.c_str());

			outputFile = prefix + p.first + "_" + p.second->title();

            excls.push_back("#p");

            gnuplotExcl = "p";
		}
		std::vector<std::pair<SimulationSettings::StatisticsElement, StatisticsOperation*>> v;
		v.push_back(std::make_pair(SimulationSettings::StatisticsElement::nK, dynamic_cast<StatisticsOperation*>(p.second)));
		dumpResults(outputFile + ext, settings, v, excls);
		settings->outputFile = outputFile + spl.back();
		if (settings->generateGnuplot)
			dumpGnuplot(outputFile + ".plt", settings, v, gnuplotExcl);

        settings->outputFile = initFile;
	}
}

std::string Statistics::toKey(SimulationSettings* settings, const std::string& excl)
{
    return settings->toString(excl);
}

void Statistics::mergeSingleResults()
{
    Statistics_TauTotal* totalTau = nullptr;
    std::map<std::string, StatisticsSingle*> totalsTaus;
    std::map<std::string, StatisticsSingle*> totalsAnys;
    for (std::map<SimulationSettings::StatisticsElement , std::map<std::string, StatisticsSingle*>>::value_type& s : mSingles)
    {
        if (s.first == SimulationSettings::StatisticsElement::tau)
        {
            for (std::map<std::string, StatisticsSingle*>::value_type& inn : s.second)
            {
                auto tau = dynamic_cast<Statistics_Tau*>(inn.second);
                auto config = StringHelper::split(inn.first, "K").front();
                if (!totalsTaus.count(config))
                {
                    totalsTaus[config] = new Statistics_TauTotal(inn.first, tau);
                }

                dynamic_cast<Statistics_TauTotal*>(totalsTaus[config])->addTau(inn.first, tau);
                delete tau;
            }
        }
        if (s.first == SimulationSettings::StatisticsElement::anyChunk)
        {
            for (std::map<std::string, StatisticsSingle*>::value_type& inn : s.second)
            {
                auto any = dynamic_cast<Statistics_AnyChunk*>(inn.second);
                auto config = StringHelper::split(inn.first, "K").front();
                if (!totalsAnys.count(config))
                {
                    totalsAnys[config] = new Statistics_AnyChunkTotal(inn.first, any);
                }

                dynamic_cast<Statistics_AnyChunkTotal*>(totalsAnys[config])->addResult(inn.first, any);
                delete any;
            }
        }
    }

    if (!totalsTaus.empty())
    {
        mSingles.erase(SimulationSettings::StatisticsElement::tau);
        mSingles[SimulationSettings::StatisticsElement::tau] = totalsTaus;
    }
    if (!totalsAnys.empty())
    {
        mSingles.erase(SimulationSettings::StatisticsElement::anyChunk);
        mSingles[SimulationSettings::StatisticsElement::anyChunk] = totalsAnys;
    }
}

bool Statistics::containsExclude(const std::string &line, const std::vector<std::string>& excls)
{
    bool ret = false;

    for (auto& s : excls)
        ret |= line.find(s) != line.npos;

    return ret;
}

std::string Statistics::toString(const int &val)
{
	return std::to_string(val * 100);
}

std::string Statistics::toString(const double &val)
{
	return std::to_string(int(val * 100));
}

template<typename T1, typename T2>
std::pair<std::string, std::string> Statistics::toPair(T1 t1, T2 t2)
{
	return std::make_pair(toString(t1), toString(t2));
}
