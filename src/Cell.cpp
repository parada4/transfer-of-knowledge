#include "Cell.h"
#include "RandomGenerator.h"
#include "SimulationSettings.h"

Cell::Cell(): mSettings(nullptr) {}

Cell::~Cell() {}

Cell::Cell(const Cell& other) : mCompetences(other.mCompetences), mSum(other.mSum)
{}

Cell::Cell(Cell&& other): mCompetences(std::move(other.mCompetences)), mSum(std::move(other.mSum))
{}

Cell& Cell::operator=(Cell& other)
{
	mCompetences = other.mCompetences;
	mSum = other.mSum;
	return *this;
}

void Cell::init(SimulationSettings* s, int id)
{
	mId = id;
	mSettings = s;
	if (!mSettings)
		return;

	mCompetences.resize((unsigned long) mSettings->competences);
	mLearnableCompetences.resize((unsigned long) mSettings->competences);
	std::generate(mCompetences.begin(), mCompetences.end(), [this] { return mSettings->p > RandomGenerator::randomDouble(); });
	mSum = int(std::count(mCompetences.begin(), mCompetences.end(), true));
	
	unsigned long size = 4;
	if (s->neighborhood == SimulationSettings::Neighborhood::Moore)
		size = 8;
	else if (s->neighborhood == SimulationSettings::Neighborhood::VonNeumannMoore)
		size = 12;

	mNeighbors.resize(size);
	mSmarters.resize(size);
}

void Cell::setNeighbor(Cell& n, int index)
{
	if (index >= mNeighbors.size())
		return;

	mNeighbors[index] = &n;
}

void Cell::update()
{
	mSmartersSum = 0;
	for(auto n : mNeighbors)
		if (isSmartest(*n))
			mSmarters[mSmartersSum++] = n;
	
	if (!mSmartersSum)
		return;

	Cell* smartetOne = nullptr;

	if (mSettings->neighborChoice == SimulationSettings::NeighborChoice::Smartest)
	{
		std::sort(mSmarters.begin(), mSmarters.begin() + mSmartersSum);
		auto max = mSmarters.front()->mSum;
		int count = int(std::count_if(mSmarters.begin(), mSmarters.begin() + mSmartersSum, [&max](Cell*c) {return c->mSum == max; }));
		
		smartetOne = mSmarters[RandomGenerator::randomInt(0, count - 1)];
	}
	else if (mSettings->neighborChoice == SimulationSettings::NeighborChoice::Random)
		smartetOne = mSmarters[RandomGenerator::randomInt(0, mSmartersSum - 1)];
	else
		smartetOne = mSmarters.front();

	mLearnableSum = 0;

	for (int i = 0; i < mSettings->competences; ++i)
		if (!(*this)[i] && (*smartetOne)[i])
			mLearnableCompetences[mLearnableSum++] = i;

	int learnIndex = 0;
	if (mSettings->competenceChoice == SimulationSettings::CompetenceChoice::RightLeft)
		learnIndex = mLearnableSum - 1;
	else if (mSettings->competenceChoice == SimulationSettings::CompetenceChoice::Random)
		learnIndex = RandomGenerator::randomInt(0, mLearnableSum - 1);

	addCompetence(mLearnableCompetences[learnIndex]);
}

void Cell::reset()
{
	std::generate(mCompetences.begin(), mCompetences.end(), [this] { return RandomGenerator::randomDouble() < mSettings->p; });
	mSum = int(std::count(mCompetences.begin(), mCompetences.end(), true));
}

long Cell::sum() const
{
	return mSum;
}

int Cell::id() const
{
	return mId;
}

void Cell::addCompetence(const int& index)
{
	mCompetences[index] = true;
	++mSum;
}

bool Cell::isSmartest(const Cell& other) const
{
	int diff = int(other - *this);

	if (diff < 0)
		return false;

	if (diff < mSettings->teachingRangeFrom || diff > mSettings->teachingRangeTo)
		return false;

	if (diff == 0)
		return !(other == *this);
	
	return true;
}

bool operator==(const Cell& lhs, const Cell& rhs)
{
	bool equals = true;
	for (int i = 0; i < lhs.mCompetences.size(); ++ i)
		equals &= lhs.mCompetences[i] == rhs.mCompetences[i];
	return equals;
//	return std::equal(lhs.mCompetences.begin(), lhs.mCompetences.end(), rhs.mCompetences.begin(), rhs.mCompetences.end());
}

bool operator!=(const Cell& lhs, const Cell& rhs)
{
	return lhs.mSum != rhs.mSum;
}

bool operator<(const Cell& lhs, const Cell& rhs)
{
	return lhs.mSum < rhs.mSum;
}

bool operator<=(const Cell& lhs, const Cell& rhs)
{
	return !(rhs < lhs);
}

bool operator>(const Cell& lhs, const Cell& rhs)
{
	return rhs < lhs;
}

bool operator>=(const Cell& lhs, const Cell& rhs)
{
	return !(lhs < rhs);
}

long operator-(const Cell& lhs, const Cell& rhs)
{
	return lhs.mSum - rhs.mSum;
}

bool Cell::operator[](const unsigned& index)
{
	return mCompetences[index];
}
