#pragma once

#include <vector>
#include <algorithm>
#include <iostream>
#include <memory>

class SimulationSettings;

class Cell
{
public:
	explicit Cell();
	~Cell();

	Cell(const Cell& other);
	Cell(Cell&& other);
	Cell& operator=(Cell& other);

	void init(SimulationSettings* s, int id);
	void setNeighbor(Cell& n, int index);

	void update();
	void reset();
	long sum() const;
	int id() const;

	friend bool operator==(const Cell& lhs, const Cell& rhs);
	friend bool operator!=(const Cell& lhs, const Cell& rhs);

	friend bool operator<(const Cell& lhs, const Cell& rhs);
	friend bool operator<=(const Cell& lhs, const Cell& rhs);
	friend bool operator>(const Cell& lhs, const Cell& rhs);
	friend bool operator>=(const Cell& lhs, const Cell& rhs);
	
	friend long operator-(const Cell& lhs, const Cell& rhs);
	bool operator[](const unsigned& index);

private:
	void addCompetence(const int& index);
	bool isSmartest(const Cell& other) const;
	
	SimulationSettings* mSettings;
	std::vector<bool> mCompetences;
	std::vector<int> mLearnableCompetences;
	int mLearnableSum;

	std::vector<Cell*> mNeighbors;
	std::vector<Cell*> mSmarters;
	int mSum = 0;
	int mSmartersSum = 0;
	
	int mId;
};
