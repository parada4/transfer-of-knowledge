#include "RandomGenerator.h"
#include <random>

double RandomGenerator::randomDouble(const double& from, const double& to)
{
	static std::random_device rd;
	static std::mt19937 mt(rd());
	std::uniform_real_distribution<double> dist(from, to);

	return dist(mt);
}

int RandomGenerator::randomInt(const int& from, int to)
{
	static std::random_device rd;
	static std::mt19937 mt(rd());
	std::uniform_int_distribution<int> dist(from, to);

	return dist(mt);
}
