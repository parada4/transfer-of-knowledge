#pragma once

#include <vector>
#include <string>
#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>

namespace StringHelper
{
	static std::vector<std::string> split(std::string str, const std::string & delimiter)
	{
		std::vector<std::string> vec;
		size_t pos = 0;
		while ((pos = str.find(delimiter)) != std::string::npos)
		{
			vec.push_back(str.substr(0, pos));
			str.erase(0, pos + delimiter.length());
		}
		if (vec.size())
			vec.push_back(str);
		return vec;
	}
	
	static bool contains(const std::string& str, const std::string& rhs)
	{
		return str.find(rhs) != std::string::npos;
	}
	
	static inline void ltrim(std::string &s) 
	{
		s.erase(s.begin(), std::find_if(s.begin(), s.end(),
			std::not1(std::ptr_fun<int, int>(std::isspace))));
	}

	static inline void rtrim(std::string &s) 
	{
		s.erase(std::find_if(s.rbegin(), s.rend(),
			std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
	}

	static inline void trim(std::string &s) 
	{
		ltrim(s);
		rtrim(s);
	}

	static inline std::string ltrimmed(std::string s) 
	{
		ltrim(s);
		return s;
	}

	static inline std::string rtrimmed(std::string s) 
	{
		rtrim(s);
		return s;
	}

	static inline std::string trimmed(std::string s) 
	{
		trim(s);
		return s;
	}
}