#pragma once

#include <vector>
#include <algorithm>
#include <sstream>
#include <iomanip>
#include <iostream>
#include "SimulationSettings.h"
#include "StringHelper.h"
#include <utility>
#include <numeric>

const int sizeChunk = 200;
const int precision = 4;

class StatisticsOperation
{
public:
	StatisticsOperation(SimulationSettings::StatisticsElement e, const int& chunksSize = 0, bool singleFile = false);
	virtual ~StatisticsOperation();

	virtual void init(SimulationSettings* settings, int size);
	virtual void startSeries();
	virtual void finishSeries();
	virtual void addChunk(int i, int t, double val);
	virtual void normalize();
	virtual std::string firstColumn() {return "t";}
	virtual std::string firstColumnItem(int i) {return std::to_string(i);}

    virtual std::string gnuplotBase(int from, const std::string& out);
    virtual std::string gnuplotLineWidth();

	virtual int size() const { return mChunksSize; }
    virtual int maxT() const { return mMaxT; }
	bool singleFile() const { return mSingleFile; }
    virtual SimulationSettings::StatisticsElement type() { return mType; }

	virtual std::string preHeaders(bool lastOne) {return "";}
	virtual std::string title() { return mTitle; };
	virtual std::string headers(bool lastOne);
	virtual std::string results(int t, bool lastOne);
    virtual std::string gnuplot(int from, const std::string& out);

    virtual bool isPercentage() { return true; }
    virtual std::string keyTitle() { return ""; }
    virtual std::string keyTitleLine();

    virtual std::string yLabelLine(const std::string &title, const std::string& offset = "3");
    virtual std::string xLabelLine(const std::string &title, const std::string& offset = "0.3");

protected:
	virtual void fill(std::vector<std::vector<double>>& vec, int offset);
	virtual void resizeAndFill();
	virtual std::string headerItem(const int& i) { return std::to_string(i); }

	std::vector<std::vector<double>> mChunks;
	std::vector<std::vector<double>> mCurrentChunks;

	int mCurrentMaxT;
	int mCurrentSize;
	int mSize;
	int mMaxT;

	int mChunksSize;
	int mSimulations;

	bool mSingleFile;
	std::string mTitle;

    SimulationSettings::StatisticsElement mType;
};

class Statistics_fk : public StatisticsOperation
{
public:
	Statistics_fk(const int& chunksSize = 0);
	virtual ~Statistics_fk();
	
	virtual std::string headerItem(const int& i) override;
	virtual std::string gnuplot(int from, const std::string& out) override;
};

class Statistics_nk : public StatisticsOperation
{
public:
	Statistics_nk(const int& chunksSize = 0);
	virtual ~Statistics_nk();

	virtual std::string gnuplot(int from, const std::string& out) override;
    virtual std::string keyTitle() { return "k"; }
};

class StatisticsSingle : public StatisticsOperation
{
public :
	StatisticsSingle(SimulationSettings::StatisticsElement e,const std::vector<double>& values);
	virtual ~StatisticsSingle();

	virtual void init(SimulationSettings* settings, const std::vector<double>& values);
   virtual std::vector<double> values();

protected:
	std::vector<double> mValues;
};

class Statistics_nK : public StatisticsSingle
{
public:
	Statistics_nK(const std::vector<double>& values);
	virtual ~Statistics_nK();

	virtual void addChunk(int i, int t, double val) override;
	virtual void finishSeries() override;
	virtual std::string headerItem(const int& i) override;
	virtual std::string gnuplot(int from, const std::string& out) override;
	virtual std::string headers(bool lastOne) override;
	virtual std::string results(int t, bool lastOne) override;
	 virtual std::string keyTitle() { return "p"; }

protected:
	int mCurrentI;
};

class Statistics_Tau : public StatisticsSingle
{
public:
	Statistics_Tau(const std::vector<double>& values);
	virtual ~Statistics_Tau();

	virtual void addChunk(int i, int t, double val) override;
	virtual void normalize() override;
	virtual std::string headerItem(const int& i) override;
	virtual std::string gnuplot(int from, const std::string& out) override;
	virtual std::string headers(bool lastOne) override;
	virtual std::string results(int t, bool lastOne) override;
	virtual std::string firstColumn();
	virtual std::string firstColumnItem(int i) override;
   virtual bool isPercentage() { return false; }

   std::vector<double> taus();
   std::vector<double> devs();

protected:
    std::vector<double> mTaus;
    std::vector<double> mDevs;
    std::vector<std::vector<double>> mTausAll;
};

class Statistics_TauTotal : public StatisticsSingle
{
public:
    Statistics_TauTotal(const std::string& config, Statistics_Tau* tau);
    Statistics_TauTotal(const std::vector<double>& values);
    virtual ~Statistics_TauTotal();


    virtual std::string firstColumn();
    virtual std::string firstColumnItem(int i) override;
    virtual std::string headers(bool lastOne) override;
    virtual std::string results(int t, bool lastOne) override;
    virtual std::string preHeaders(bool lastOne) override;
    virtual std::string gnuplot(int from, const std::string& out) override;
    virtual bool isPercentage() { return false; }
    virtual std::string keyTitle() { return "K="; }

    void addTau(const std::string& config, Statistics_Tau* tau);
    std::string config();

protected:
    std::string mConfig;
    std::map<int, std::pair<std::vector<double>, std::vector<double>>> mTaus;
};

class Statistics_AnyChunk : public StatisticsSingle
{
public:
    Statistics_AnyChunk(const std::vector<double>& values);
    virtual ~Statistics_AnyChunk();

    virtual void addChunk(int i, int t, double val) override;
    virtual void normalize() override;
    virtual std::string headerItem(const int& i) override;
    virtual std::string gnuplot(int from, const std::string& out) override;
    virtual std::string headers(bool lastOne) override;
    virtual std::string results(int t, bool lastOne) override;
    virtual std::string firstColumn();
    virtual std::string firstColumnItem(int i) override;

    std::vector<double> taus();

protected:
    std::vector<double> mTaus;
};

class Statistics_AnyChunkTotal : public StatisticsSingle
{
public:
    Statistics_AnyChunkTotal(const std::string& config, Statistics_AnyChunk* any);
    Statistics_AnyChunkTotal(const std::vector<double>& values);
    virtual ~Statistics_AnyChunkTotal();

    virtual std::string firstColumn();
    virtual std::string firstColumnItem(int i) override;
    virtual std::string headers(bool lastOne) override;
    virtual std::string results(int t, bool lastOne) override;
    virtual std::string preHeaders(bool lastOne) override;
    virtual std::string gnuplot(int from, const std::string& out) override;
    virtual std::string keyTitle() { return "K"; }

    void addResult(const std::string& config, Statistics_AnyChunk* any);
    std::string config();

protected:
    std::string mConfig;
    std::map<int, std::vector<double>> mResults;
};

class Statistics_nKvsLp : public StatisticsSingle
{
public:
    Statistics_nKvsLp(const std::vector<double>& ps);
    virtual ~Statistics_nKvsLp();

    virtual void init(SimulationSettings* settings, int size);
    virtual void startSeries();
    virtual void finishSeries();
    virtual void addChunk(int i, int t, double val);
    virtual void normalize();
    virtual int size() const;
    virtual int maxT() const;
    virtual std::string gnuplot(int from, const std::string& out) override;
    virtual std::string headerItem(const int& i) override;
    virtual std::string headers(bool lastOne) override;
    virtual std::string results(int t, bool lastOne) override;
	 virtual std::string keyTitle() { return "L="; }

protected:
    std::vector<double> mData;
};