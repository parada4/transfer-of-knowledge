#pragma once

#include "SimulationSettings.h"
#include <string>
#include <array>
#include <vector>
#include <functional>	

class ArgumentsParser
{
public:
	explicit ArgumentsParser();
	virtual ~ArgumentsParser();
	
	bool validate(int argc, char** argv);
	SimulationSettings settings() const;
	bool next();
	int size();
	int current();

	static std::string generateFilename(const std::string& output, const std::string& input, const std::string& settings);

	static const int sFileLines = 9;

private:
	static bool error(const std::string& msg);
	bool tryParseFile(const std::string& filename);

	template<typename T>
	void getParameter(std::string& line, T& p, SimulationSettings::RangeVariable r);
	
	void setCurrentStep();

	static void autoGenerateFile(const std::string &filename = "input.dat");

	SimulationSettings mSettings;
	std::vector<std::function<void(std::string)>> mLinesInterpeter;
	std::vector<std::vector<std::function<void()>>> mSteps;
	bool mError;
	int mStepsCounter = -1;
	int mStepsSize;
	std::vector<int> mStepsCounters;
};