#pragma once
#include <vector>
#include <string>
#include <map>
#include <functional>	
#include "StatisticsOperation.h"

class SimulationSettings;

class Statistics
{
public:
	~Statistics();

	void init(SimulationSettings* settings);
	void dumpGlobalResults(const std::string& outputFile, SimulationSettings* settings);
	void dumpResults(const std::string& outputFile, SimulationSettings* settings, std::vector<std::pair<SimulationSettings::StatisticsElement, StatisticsOperation*>>& vec, const std::string& excl = "");
	void dumpResults(const std::string& outputFile, SimulationSettings* settings, std::vector<std::pair<SimulationSettings::StatisticsElement, StatisticsOperation*>>& vec, const std::vector<std::string>& excl);
	void dumpGlobalGnuplot(const std::string& outputFile, SimulationSettings* settings);
	void dumpGnuplot(const std::string& gnuplotFile, SimulationSettings* settings, std::vector<std::pair<SimulationSettings::StatisticsElement, StatisticsOperation*>>& vec, const std::string& excl = "");
	void dumpSingleResults(SimulationSettings* settings);
	void dumpSingleFileResults(SimulationSettings* settings, SimulationSettings::RangeVariable r, std::map<std::string, StatisticsSingle*>& map);

	void mergeSingleResults();

	void startSeries();
	void finishSeries();
	void normalize();

	void startSingleSeries(SimulationSettings* settings);
	void finishSingleSeries();
	void normalizeSingle();

	void addChunk(SimulationSettings::StatisticsElement e, int K, int t, double val);
	void addSingleChunk(SimulationSettings * s, SimulationSettings::StatisticsElement e,SimulationSettings::RangeVariable r, int i, int t, double val);

	static Statistics& sInstance();

private:
	bool hasOperation(SimulationSettings::StatisticsElement e) { return mMapIndexes.count(e) > 0;}
	bool lastOperation(SimulationSettings::StatisticsElement e);

	bool containsExclude(const std::string& line, const std::vector<std::string>& excls);

	int mMaxT;
	std::vector<std::pair<SimulationSettings::StatisticsElement, StatisticsOperation*>> mOperations;

	std::vector<std::pair<SimulationSettings::StatisticsElement, StatisticsOperation*>> mGlobalOperations;
	std::vector<std::pair<SimulationSettings::StatisticsElement, StatisticsSingle*>> mSignleOperations;

	std::map<SimulationSettings::StatisticsElement, size_t> mMapIndexes;

	std::map<SimulationSettings::StatisticsElement, std::map<std::string, StatisticsSingle*>> mSingles;

	static std::map<SimulationSettings::StatisticsElement, std::pair<std::function<StatisticsOperation*(SimulationSettings*)>, std::function<void(StatisticsOperation*,SimulationSettings*)>>> sOperations;

	std::string toString(const int& val);
	std::string toString(const double& val);

	template<typename T1, typename T2>
	std::pair<std::string, std::string> toPair(T1 t1, T2 t2);

	std::string toKey(SimulationSettings* settings, const std::string& excl = "");
};

