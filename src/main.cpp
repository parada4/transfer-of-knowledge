#include "ArgumentsParser.h"
#include "Simulation.h"
#include "Statistics.h"
#include <fstream>

void threadJob(SimulationSettings* settings, int runs)
{
	Simulation s(settings);
	for (int i = 0; i < runs; ++i)
	{
		s.run();
		s.reset();
	}
}

int main(int argc, char** argv)
{
	ArgumentsParser parser;
	if (parser.validate(argc, argv))
	{
		auto s = parser.settings();
		int threadsNum = s.threads; 
		bool multiThread = threadsNum > 1;
		int runsPerThread = s.simulations / threadsNum;
		std::vector<int> threadsRuns((unsigned long) threadsNum, runsPerThread);
		int diff = s.simulations - runsPerThread * threadsNum;
		for(int i = 0; i < threadsNum; ++i)
		{
			++threadsRuns[i];
			if (--diff == 0)
				break;
		}

		std::cout << "[----------] 0%";
		auto start = std::chrono::system_clock::now();
		auto size = parser.size();
		int lastf = 0;

		while (parser.next())
		{
			SimulationSettings settings = std::move(parser.settings());
			Statistics::sInstance().init(&settings);
			Statistics::sInstance().startSingleSeries(&settings);

			if (multiThread)
			{
				std::vector<std::thread> threads;
				for(int i = 0; i < threadsNum; ++i)
					threads.push_back(std::thread(threadJob,&settings, threadsRuns[i]));

				for (auto& t : threads)
					t.join();
	
				int f = int(100 * (parser.current() * double(settings.simulations)) / (double(settings.simulations) * size));
				if (f >= lastf + 10)
				{
					lastf = f;
					std::cout << '\n' << "[";
					for (int i = 0; i < 10; ++i)
						std::cout << (i <= f / 10 -1 ? '*' : '-');
					std::cout << "] " << f << "%";
				}
			}
			else
			{
				Simulation simulation(&settings);
	
				auto lastF = 0;
				for (int i = 0; i < settings.simulations; ++i)
				{
					simulation.run();
					simulation.reset();
					
					int f = int(100 * (parser.current() * double(settings.simulations) + i) / (double(settings.simulations) * size));
				   if (f >= lastF + 10)
					{
						lastF = f;
						std::cout << '\n' << "[";
						for (int j = 0; j < 10; ++j)
							std::cout << (j <= f / 10 -1 ? '*' : '-');
						std::cout << "] " << f << "%";
					}
				}
			}
			

			if (settings.ranges[SimulationSettings::RangeVariable::p].size() > 0 && std::fabs(settings.p - settings.ranges[SimulationSettings::RangeVariable::p].back()) < 0.00001)
				continue;

			settings.outputFile =  ArgumentsParser::generateFilename(settings.outputFile, settings.inputFile, settings.toString());
			auto dot = settings.outputFile.find_last_of(".");
			if (dot != settings.outputFile.npos)
				settings.gnuplotFile = settings.outputFile.substr(0, dot) + ".plt";
			else
				settings.gnuplotFile = settings.outputFile + ".plt";

			Statistics::sInstance().normalize();
			Statistics::sInstance().dumpGlobalResults(settings.outputFile, &settings);
			if (settings.generateGnuplot)
				Statistics::sInstance().dumpGlobalGnuplot(settings.gnuplotFile, &settings);
		}

		Statistics::sInstance().normalizeSingle();
		auto settings = std::move(parser.settings());

		settings.outputFile = ArgumentsParser::generateFilename(settings.outputFile, settings.inputFile, "");

		auto dot = settings.outputFile.find_last_of(".");
		if (dot != settings.outputFile.npos)
			settings.gnuplotFile = settings.outputFile.substr(0, dot) + ".plt";
		else
			settings.gnuplotFile = settings.outputFile + ".plt";


        Statistics::sInstance().mergeSingleResults();
        Statistics::sInstance().dumpSingleResults(&settings);

		auto end = std::chrono::system_clock::now();
		std::chrono::duration<double, std::milli> dur = end - start;
		std::cout << "\nExecution time: " << dur.count() << " ms" << std::endl;
	}

	return 0;
}

