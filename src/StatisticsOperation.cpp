#include "StatisticsOperation.h"

StatisticsOperation::StatisticsOperation(SimulationSettings::StatisticsElement e, const int& chunksSize, bool singleFile)
	: mChunksSize(chunksSize), mSingleFile(singleFile), mType(e)
{
	mTitle = "f(k)";
	for (std::map<std::string, SimulationSettings::StatisticsElement>::value_type&s : SimulationSettings::sStatisticsMap)
		if (s.second == e)
			mTitle = s.first;
}

StatisticsOperation::~StatisticsOperation()
{
}

void StatisticsOperation::init(SimulationSettings * settings, int size)
{
	mChunksSize = size;
	mSimulations = settings->simulations;
	mSize = mCurrentSize = sizeChunk;

	mChunks.resize((unsigned long) mChunksSize);
	mCurrentChunks.resize((unsigned long) mChunksSize);

	mMaxT = mCurrentMaxT = 0;

	for (int i = 0; i < mChunksSize; ++i)
	{
		mChunks[i].resize(sizeChunk);
		mCurrentChunks[i].resize(sizeChunk);
		std::fill(mChunks[i].begin(), mChunks[i].end(), 0.0);
		std::fill(mCurrentChunks[i].begin(), mCurrentChunks[i].end(), 0.0);
	}
}

void StatisticsOperation::startSeries()
{
	mCurrentMaxT = 0;
}

void StatisticsOperation::finishSeries()
{
	if (mCurrentMaxT >= mSize)
		resizeAndFill();
	else if (mCurrentMaxT > mMaxT && mMaxT != 0)
		fill(mChunks, mMaxT);
	else if (mCurrentMaxT < mMaxT)
		fill(mCurrentChunks, mCurrentMaxT);

	if (mCurrentMaxT > mMaxT)
		mMaxT = mCurrentMaxT;

	for (int i = 0; i < mChunksSize; ++i)
	{
		for (int j = 0; j <= mMaxT; ++j)
		{
			mChunks[i][j] += mCurrentChunks[i][j];
		}
	}
}

void StatisticsOperation::addChunk(int i, int t, double val)
{
   if (isPercentage())
      val *= 100;

	if (t >= mCurrentSize)
	{
		mCurrentSize += sizeChunk;
		for (auto& v : mCurrentChunks)
			v.resize((unsigned long) mCurrentSize);
	}
	mCurrentChunks[i][t] = val;

	mCurrentMaxT = t;
}

void StatisticsOperation::normalize()
{
	for(auto& v : mChunks)
	{
		std::transform(v.begin(), v.end(), v.begin(), [this](double& d)
		{
			return d / mSimulations;
		});
	}
}

std::string StatisticsOperation::gnuplotBase(int from, const std::string& out)
{
   auto str = keyTitleLine();
   str += yLabelLine(mTitle);
   str +=  isPercentage() ? " set yrange[0:100]\n" : "set yrange[0:1]\n";
   str += "plot for [col=" + std::to_string(from) + ":" + std::to_string(from + mChunksSize - 1) + "] '" + out + "'";
   return str;
}

std::string StatisticsOperation::gnuplotLineWidth()
{
   return " pointsize 4 lw 4 ";
}

std::string StatisticsOperation::headers(bool lastOne)
{
	std::string str;
	for (int i = 0; i < mChunksSize; ++i)
	{
		auto spl = StringHelper::split(mTitle, "(");
		str += spl.size() > 0 ? spl.front() : "f";
		str += "(" + headerItem(i) + ")";
		if (!lastOne)
			str += "\t";
		else
		{
			if (i != mChunksSize - 1)
				str += "\t";
			else
				str += "\n";
		}
	}
	return str;
}

std::string StatisticsOperation::results(int t, bool lastOne)
{
	std::stringstream ss;
	for (int i = 0; i < mChunksSize; ++i)
	{
		ss << std::fixed << std::setprecision(precision) << mChunks[i][t];
		if (!lastOne)
			ss << "\t";
		else
		{
			if (i != mChunksSize - 1)
				ss << "\t";
			else
				ss << "\n";
		}
	}
	return ss.str();
}

std::string StatisticsOperation::gnuplot(int from, const std::string& out)
{
	return gnuplotBase(from, out);
}

void StatisticsOperation::fill(std::vector<std::vector<double>>& vec, int offset)
{
	for(auto& v : vec)
		std::fill(v.begin() + offset + 1, v.end(), v[offset]);
}

void StatisticsOperation::resizeAndFill()
{
	int diff = mCurrentSize - mSize;
	for(auto& v : mChunks)
	{
		v.resize((unsigned long) mCurrentSize);
		std::fill_n(v.end() - diff, diff, v[mCurrentMaxT]);
	}

	mSize = mCurrentSize;
}

std::string StatisticsOperation::keyTitleLine()
{
   return keyTitle() == "" ? "" : "set key title '{/Times-Italic " + keyTitle() + "=}'\n";
}

std::string StatisticsOperation::yLabelLine(const std::string &title, const std::string& offset)
{
   return "set ylabel '{/Times-Italic " + title + " }" +  (isPercentage() ? "[%]" : "") + "' offset " + offset + "\n";
}

std::string StatisticsOperation::xLabelLine(const std::string &title, const std::string& offset)
{
   return "set xlabel '{/Times-Italic " + title + "}' offset 0," + offset + "\n";
}

Statistics_fk::Statistics_fk(const int &chunksSize) :StatisticsOperation(SimulationSettings::StatisticsElement::fk, chunksSize, false)
{}

Statistics_fk::~Statistics_fk()
{}

std::string Statistics_fk::headerItem(const int &i)
{
   return std::to_string(i + 1);
}

std::string Statistics_fk::gnuplot(int from, const std::string &out)
{
	std::string str = StatisticsOperation::gnuplot(from, out);
	str += " using 1:col with lp title 'c_{'.(col+1-" + std::to_string(from) + ").'}' ";
   str += gnuplotLineWidth() + "\n";
	return str;
}

Statistics_nk::Statistics_nk(const int &chunksSize) :StatisticsOperation(SimulationSettings::StatisticsElement::nk, chunksSize, false)
{}

Statistics_nk::~Statistics_nk()
{}

std::string Statistics_nk::gnuplot(int from, const std::string &out)
{
   std::string str = StatisticsOperation::gnuplot(from, out);
   str += " using 1:col with lp title ''.(col-" + std::to_string(from) + ") ";
   str += gnuplotLineWidth() + "\n";
   return str;
}

StatisticsSingle::StatisticsSingle(SimulationSettings::StatisticsElement e, const std::vector<double> &values)
        :StatisticsOperation(e, int(values.size()), true), mValues(values)
{}

StatisticsSingle::~StatisticsSingle()
{}

void StatisticsSingle::init(SimulationSettings *settings, const std::vector<double> &values)
{
   StatisticsOperation::init(settings, int(values.size()));
}

std::vector<double> StatisticsSingle::values()
{
   return mValues;
}


Statistics_nK::Statistics_nK(const std::vector<double> &values) :StatisticsSingle(SimulationSettings::StatisticsElement::nK, values)
{}

Statistics_nK::~Statistics_nK()
{}

void Statistics_nK::addChunk(int i, int t, double val)
{
   StatisticsSingle::addChunk(i, t, val);
   mCurrentI = i;
}

void Statistics_nK::finishSeries()
{
   if (mCurrentMaxT >= mSize)
      resizeAndFill();
   else if (mCurrentMaxT > mMaxT && mMaxT != 0)
      fill(mChunks, mMaxT);
   else if (mCurrentMaxT < mMaxT)
      std::fill(mCurrentChunks[mCurrentI].begin() + mCurrentMaxT + 1, mCurrentChunks[mCurrentI].end(), mCurrentChunks[mCurrentI][mCurrentMaxT]);

   if (mCurrentMaxT > mMaxT)
      mMaxT = mCurrentMaxT;

   for (int j = 0; j <= mMaxT; ++j)
   {
      mChunks[mCurrentI][j] += mCurrentChunks[mCurrentI][j];
   }
}

std::string Statistics_nK::headerItem(const int &i)
{
   int perc = 1;
   std::stringstream ss;
   for (int i = 0; i < mValues.size(); ++i)
   {
      ss << std::fixed << std::setprecision(4) << mValues[i];
      if (ss.str()[3] != '0' && perc == 1)
         perc = 2;
      ss.str("");
   }

   ss << std::fixed << std::setprecision(perc) << mValues[i];
   return ss.str();
}

std::string Statistics_nK::gnuplot(int from, const std::string &out)
{
   std::string str = yLabelLine("n(K)") + " plot ";

   for (int i = 0; i < mChunksSize - 1; ++i)
   {
      str += "'" + out + "' using 0:" + std::to_string(from+i) + " t'p(" + headerItem(i) + ")' with linespoints " + gnuplotLineWidth();
      if (i == mChunksSize - 2)
         str += "\n";
      else
         str += ", ";

   }

   return str;
}

std::string Statistics_nK::headers(bool lastOne)
{
   std::string str;
   for (int i = 0; i < mChunksSize - 1; ++i)
   {
      str += "p=" + headerItem(i);
      if (!lastOne)
         str += "\t";
      else
      {
         if (i != mChunksSize - 2)
            str += "\t";
         else
            str += "\n";
      }
   }
   return str;
}

std::string Statistics_nK::results(int t, bool lastOne)
{
   std::stringstream ss;
   for (int i = 0; i < mChunksSize - 1; ++i)
   {
      ss << std::fixed << std::setprecision(precision) << mChunks[i][t];
      if (!lastOne)
         ss << "\t";
      else
      {
         if (i != mChunksSize - 2)
            ss << "\t";
         else
            ss << "\n";
      }
   }
   return ss.str();
}

Statistics_Tau::Statistics_Tau(const std::vector<double> &values) :StatisticsSingle(SimulationSettings::StatisticsElement::tau, values)
{
   mTaus.resize(values.size());
   mTausAll.resize(values.size());
   mDevs.resize(values.size());
}

Statistics_Tau::~Statistics_Tau()
{}

void Statistics_Tau::addChunk(int i, int t, double val)
{
   mTaus[i] += val;
   mTausAll[i].push_back(val);
}

void Statistics_Tau::normalize()
{
   for (int i = 0; i < mTaus.size(); ++i)
   {
      mTaus[i] = mTaus[i] / mSimulations;

      auto& v = mTausAll[i];
      double sum = std::accumulate(v.begin(), v.end(), 0.0);
      double mean = sum / v.size();

      std::vector<double> diff(v.size());
      std::transform(v.begin(), v.end(), diff.begin(), [mean](double x) { return x - mean; });
      double sq_sum = std::inner_product(diff.begin(), diff.end(), diff.begin(), 0.0);
      double stdev = std::sqrt(sq_sum / (v.size() * (v.size() - 1)));

      mDevs[i] = stdev;
   }

   mMaxT = int(mTaus.size() - 1);

}

std::string Statistics_Tau::headerItem(const int &i)
{
   std::stringstream ss; ss << std::fixed << std::setprecision(1) << mValues[i];
   return ss.str();
}

std::string Statistics_Tau::gnuplot(int from, const std::string &out)
{
   std::string str = yLabelLine("{/Symbol t}") + xLabelLine("p") +
           "set autoscale xy\nplot ";

   for (int i = 0; i < mChunksSize - 1; ++i)
   {
      str += "'" + out + "' using 0:" + std::to_string(from+i) + " t'p(" + headerItem(i) + ")' ps 10 " + gnuplotLineWidth();
      if (i == mChunksSize - 2)
         str += "\n";
      else
         str += ", ";
   }

   return str;
}

std::string Statistics_Tau::headers(bool lastOne)
{
   std::string str = "\t";
   for (int i = 0; i < 1; ++i)
   {
      auto spl = StringHelper::split(mTitle, "(");
      str += spl.size() > 0 ? spl.front() : "f";
      str += "(" + headerItem(i) + ")";
      str += "\n";
   }
   return str;
}

std::string Statistics_Tau::results(int t, bool lastOne)
{
   std::stringstream ss;
   ss << std::fixed << std::setprecision(precision) << mTaus[t];
   return ss.str() + "\n";
}

std::string Statistics_Tau::firstColumn()
{
   return "p";
}

std::string Statistics_Tau::firstColumnItem(int i)
{
   std::stringstream ss;
   ss << std::fixed << std::setprecision(2) << mValues[i];
   return ss.str();
}

std::vector<double> Statistics_Tau::taus()
{
   return mTaus;
}

std::vector<double> Statistics_Tau::devs()
{
   return mDevs;
}


Statistics_TauTotal::Statistics_TauTotal(const std::string &config, Statistics_Tau *tau) :Statistics_TauTotal(tau->values())
{
   mConfig = config;
   mMaxT = tau->maxT();
}

Statistics_TauTotal::Statistics_TauTotal(const std::vector<double> &values) :StatisticsSingle(SimulationSettings::StatisticsElement::tau, values)
{}

Statistics_TauTotal::~Statistics_TauTotal()
{}

void Statistics_TauTotal::addTau(const std::string &config, Statistics_Tau *tau)
{
   auto spl = StringHelper::split(config, "K");
   int K = atoi(spl.back().c_str());

   mTaus[K] = std::make_pair(tau->devs(), tau->taus());
}

std::string Statistics_TauTotal::config()
{
   return mConfig;
}

std::string Statistics_TauTotal::firstColumn()
{
   return "p";
}

std::string Statistics_TauTotal::firstColumnItem(int i)
{
   std::stringstream ss;
   ss << std::fixed << std::setprecision(2) << mValues[i];
   return ss.str();
}

std::string Statistics_TauTotal::headers(bool lastOne)
{
   std::string str;

   for (int i = 0; i < mTaus.size(); ++i)
   {
      str += "tau\tstddev\t";
   }
   str += "\n";

   return str;
}

std::string Statistics_TauTotal::results(int t, bool lastOne)
{
   std::stringstream ss;
   std::string str;
   for (std::map<int, std::pair<std::vector<double>, std::vector<double>>>::value_type& inn : mTaus)
   {
      ss << std::fixed << std::setprecision(precision) << inn.second.second[t] << "\t" << std::fixed << std::setprecision(precision) << inn.second.first[t] << "\t";
   }
   return ss.str() + "\n";
}

std::string Statistics_TauTotal::preHeaders(bool lastOne)
{
   std::string str = "#\t";

   for (std::map<int, std::pair<std::vector<double>, std::vector<double>>>::value_type& inn : mTaus)
   {
      str += "k=\t" + std::to_string(inn.first) + "\t";
   }
   return str + "\n";
}

std::string Statistics_TauTotal::gnuplot(int from, const std::string &out)
{
   std::string str = yLabelLine("{/Symbol t}", "2") + xLabelLine("p") +
           "set autoscale xy\n" +
           "set xrange [0:1]\nplot ";

   int i = 0;
   for (std::map<int, std::pair<std::vector<double>, std::vector<double>>>::value_type& inn : mTaus)
   {
      str += "'" + out + "' using 1:" + std::to_string(2*(i+1)) + " t'" + std::to_string(inn.first) + "' with linespoints " + gnuplotLineWidth();
      if (i == mTaus.size() - 1)
         str += "\n";
      else
         str += ", ";
      ++i;
   }

   return str;
}

Statistics_AnyChunk::Statistics_AnyChunk(const std::vector<double> &values) :StatisticsSingle(SimulationSettings::StatisticsElement::anyChunk, values)
{
   mTaus.resize(values.size());
}

Statistics_AnyChunk::~Statistics_AnyChunk()
{}

void Statistics_AnyChunk::addChunk(int i, int t, double val)
{
   mTaus[i] += val;
}

void Statistics_AnyChunk::normalize()
{
   for (int i = 0; i < mTaus.size(); ++i)
   {
      mTaus[i] = mTaus[i] / mSimulations;
   }

   mMaxT = mTaus.size()-1;

}

std::string Statistics_AnyChunk::headerItem(const int &i)
{
   std::stringstream ss; ss << std::fixed << std::setprecision(1) << mValues[i];
   return ss.str();
}

std::string Statistics_AnyChunk::gnuplot(int from, const std::string &out)
{
   std::string str = yLabelLine("{/Symbol \341} f {/Symbol \361}") + xLabelLine("p") +
           "set autoscale xy\nplot ";

   for (int i = 0; i < mChunksSize - 1; ++i)
   {
      str += "'" + out + "' using 0:" + std::to_string(from+i) + " t'p(" + headerItem(i) + ")' ps 10 " + gnuplotLineWidth();
      if (i == mChunksSize - 2)
         str += "\n";
      else
         str += ", ";
   }

   return str;
}

std::string Statistics_AnyChunk::headers(bool lastOne)
{
   std::string str = "\t";
   for (int i = 0; i < 1; ++i)
   {
      auto spl = StringHelper::split(mTitle, "(");
      str += spl.size() > 0 ? spl.front() : "f";
      str += "(" + headerItem(i) + ")";
      str += "\n";
   }
   return str;
}

std::string Statistics_AnyChunk::results(int t, bool lastOne)
{
   std::stringstream ss;
   ss << std::fixed << std::setprecision(precision) << mTaus[t];
   return ss.str() + "\n";
}

std::string Statistics_AnyChunk::firstColumn()
{
   return "p";
}

std::string Statistics_AnyChunk::firstColumnItem(int i)
{
   std::stringstream ss;
   ss << std::fixed << std::setprecision(2) << mValues[i];
   return ss.str();
}

std::vector<double> Statistics_AnyChunk::taus()
{
   return mTaus;
}


Statistics_AnyChunkTotal::Statistics_AnyChunkTotal(const std::string &config, Statistics_AnyChunk *any) :Statistics_AnyChunkTotal(any->values())
{
   mConfig = config;
   mMaxT = any->maxT();
}

Statistics_AnyChunkTotal::Statistics_AnyChunkTotal(const std::vector<double> &values) :StatisticsSingle(SimulationSettings::StatisticsElement::anyChunk, values)
{}

Statistics_AnyChunkTotal::~Statistics_AnyChunkTotal()
{}

void Statistics_AnyChunkTotal::addResult(const std::string &config, Statistics_AnyChunk *any)
{
   auto spl = StringHelper::split(config, "K");
   int K = atoi(spl.back().c_str());

   mResults[K] = any->taus();
}

std::string Statistics_AnyChunkTotal::config() { return mConfig; }

std::string Statistics_AnyChunkTotal::firstColumn() {return "p";}

std::string Statistics_AnyChunkTotal::firstColumnItem(int i) {
   std::stringstream ss;
   ss << std::fixed << std::setprecision(2) << mValues[i];
   return ss.str();
}

std::string Statistics_AnyChunkTotal::headers(bool lastOne) {
   std::string str;

   for (int i = 0; i < mResults.size(); ++i)
   {
      str += "any\t";
   }
   str += "\n";

   return str;
}

std::string Statistics_AnyChunkTotal::results(int t, bool lastOne) {
   std::stringstream ss;
   std::string str;
   for (std::map<int, std::vector<double>>::value_type& inn : mResults)
   {
      ss << std::fixed << std::setprecision(precision) << inn.second[t] << "\t";
   }
   return ss.str() + "\n";
}

std::string Statistics_AnyChunkTotal::preHeaders(bool lastOne) {
   std::string str = "#\t";

   for (std::map<int, std::vector<double>>::value_type& inn : mResults)
   {
      str += "k=" + std::to_string(inn.first) + "\t";
   }
   return str + "\n";
}

std::string Statistics_AnyChunkTotal::gnuplot(int from, const std::string &out) {
   std::string str = yLabelLine("{\341 f \361}") + xLabelLine("p") + keyTitleLine() + "set autoscale xy\nplot ";

   int i = 0;
   std::string plotLine;
   for (std::map<int, std::vector<double>>::value_type& inn : mResults)
   {
      plotLine += "'" + out + "' using 1:" + std::to_string(from+i) + " t'" + std::to_string(inn.first) + "' with linespoints " + gnuplotLineWidth();
      if (i == mResults.size() - 1)
         plotLine += "\n";
      else
         plotLine += ", ";
      ++i;
   }
   str += plotLine;
   str += "set yrange[90:100]\n";
   auto spl = StringHelper::split(out, ".").front();
   spl += "_zoom.eps";
   str += "set out '" + spl + "'\n";
   str += "plot " + plotLine;

   return str;
}


Statistics_nKvsLp::Statistics_nKvsLp(const std::vector<double> &ps) :StatisticsSingle(SimulationSettings::StatisticsElement::nKvsLp, ps)
{}

Statistics_nKvsLp::~Statistics_nKvsLp()
{}

void Statistics_nKvsLp::init(SimulationSettings *settings, int size)
{
   mSimulations = settings->simulations;
}

void Statistics_nKvsLp::startSeries()
{
   mData.resize(mValues.size());
}

void Statistics_nKvsLp::finishSeries()
{}

void Statistics_nKvsLp::addChunk(int i, int t, double val)
{
   mData[i] += 100 * val;
}

void Statistics_nKvsLp::normalize()
{
   for (auto& v : mData)
      v /= double(mSimulations);
}

int Statistics_nKvsLp::size() const
{
   return int(mData.size());
}

int Statistics_nKvsLp::maxT() const
{
   return 1;
}

std::string Statistics_nKvsLp::gnuplot(int from, const std::string &out)
{
   std::string str = yLabelLine("n(K)") + "plot ";

   for (int i = 0; i < mChunksSize - 1; ++i)
   {
      str += "'" + out + "' using 0:" + std::to_string(from+i) + " t'p(" + headerItem(i) + ")' with linespoints " + gnuplotLineWidth();
      if (i == mChunksSize - 2)
         str += "\n";
      else
         str += ", ";

   }

   return str;
}

std::string Statistics_nKvsLp::headerItem(const int &i)
{
   int perc = 1;
   std::stringstream ss;
   for (int i = 0; i < mValues.size(); ++i)
   {
      ss << std::fixed << std::setprecision(4) << mValues[i];
      if (ss.str()[3] != '0' && perc == 1)
         perc = 2;
      ss.str("");
   }

   ss << std::fixed << std::setprecision(perc) << mValues[i];
   return ss.str();
}

std::string Statistics_nKvsLp::headers(bool lastOne)
{
   std::string str;
   for (int i = 0; i < mValues.size() - 1; ++i)
   {
      auto spl = StringHelper::split(mTitle, "(");
      str += spl.size() > 0 ? spl.front() : "f";
      str += "(" + headerItem(i) + ")";
      if (!lastOne)
         str += "\t";
      else
      {
         if (i != mValues.size() - 2)
            str += "\t";
         else
            str += "\n";
      }
   }
   return str + "\n";
}

std::string Statistics_nKvsLp::results(int t, bool lastOne)
{
   std::stringstream ss;
   for (int i = 0; i < mData.size() - 1; ++i)
   {
      ss << std::fixed << std::setprecision(precision) << mData[i];
      if (!lastOne)
         ss << "\t";
      else
      {
         if (i != mData.size() - 2)
            ss << "\t";
         else
            ss << "\n";
      }
   }
   return ss.str();
}

