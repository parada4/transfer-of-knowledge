\chapter{Aplikacja konsolowa}
\label{cha:console}

Drugim etapem pracy implementacyjnej było stworzenie wydajnej aplikacji konsolowej. Jej głównymi cechami powinna być niezawodność oraz szybkość. Są to kluczowe kwestie dla efektywnego przeprowadzania wielu symulacji. Generowanie dużej ilości danych potrzebnych do uzyskania odpowiednio dobrej statystyki z wykorzystaniem aplikacji internetowej nie jest optymalnym rozwiązaniem. Zaletą programu konsolowego na pewno jest możliwość uruchomienia go w tle lub na specjalnym zdalnym serwerze obliczeniowym. Program po zakończeniu działania powinien wygenerować pliki z danymi, które następnie mogą być szczegółowo analizowane. W tym celu wybrano język programowania \textit{C++}, który charakteryzuje się bardzo wysoką wydajnością. W przeciwieństwie do swojego poprzednika, języka \textit{C}, umożliwia on programowanie obiektowe. Paradygmaty programowania obiektowego w istotny sposób ułatwiają pracę programisty. Jego zaletami są m.in. łatwa rozbudowa i optymalizacja kodu, wielokrotne wykorzystywanie tych samych fragmentów programu lub przejrzystość projektu. Użyto standardu \text{C++14}, który oferuje wiele udogodnień dla twórców oprogramowania. Przy pracy nad aplikacją wykorzystano dwa zintegrowane środowiska programistyczne: \textit{CLion} \cite{CLion} oraz \textit{Microsoft Visual Studio 2015} \cite{VS}. Program był budowany z użyciem kilku kompilatorów m.in. \textit{G++}, \textit{clang} oraz kompilator środowiska \textit{Visual Studio}. W projekcie znajdującym się na dołączonej płycie CD oraz w repozytorium pracy (\url{https://bitbucket.org/parada4/transfer-of-knowledge}) znajduje się zarówno plik \textit{makefile}, służący do budowania programu w środowisku \textit{UNIX}, jak i pliki projektów dla platformy firmy \textit{Microsoft}.

\section{Parametry programu}
Program konsolowy uruchamiany jest z poziomu linii poleceń. Poniżej przedstawiono zestaw możliwych parametrów jego uruchomienia:

\begin{itemize}
\renewcommand\labelitemi{$\bullet$}
\item -\,-help --- wyświetla poradnik programu,
\item -\,-gen [\textit{nazwaPliku = input.dat}] --- generuje przykładowy plik wejściowy o wskazanej nazwie. Argument \textit{nazwaPliku} jest opcjonalny. W sytuacji, gdy nie jest on przekazany, używana jest domyślna wartość argumentu.
\item -o [\textit{nazwaPliku = results.txt}] --- dane wyjściowe zostaną umieszczone w plikach, których nazwy rozpoczynają się od \textit{nazwaPliku} nie wliczając rozszerzenia. 
\item -p [\textit{nazwaPliku = gnuplot.plt}] --- analogicznie do parametru \textit{-o} dla plików programu \textit{gnuplot} \cite{gnuplot},
\item -eps --- tworzone przez program skrypty \textit{.plt} generują pliki z rozszerzeniem \textit{.eps} (ang. \textit{Encapsulated PostScript}). Domyślnym formatem jest format \textit{PNG} (ang. \textit{Portable Network Graphics}).
\item -s [\textit{M} = 100] --- każda konfiguracja parametrów modelu zostanie uruchomiona \textit{M} razy,
\item -t [\textit{t} = 1] --- program zostanie wykonany jednocześnie na \textit{n} wątkach. Zrównoleglenie obliczeń numerycznych jest kluczowym czynnikiem w kwestii wydajności aplikacji. Pozwala ono na znaczne skrócenie czasu wykonywania, w szczególności przy dużej ilości niezależnych symulacji. Każdy z wątków przeprowadza średnio $M/t$ symulacji, po czym osobne rezultaty zostają połączone i uśrednione.   
\end{itemize} 

Przykładowe wywołanie pliku:
\begin{lstlisting}[style=plain, xleftmargin=.2\textwidth, xrightmargin=.2\textwidth]
./program -o output.dat -p -s 10000 -t 4 input.dat
\end{lstlisting}

\section{Najważniejsze klasy}
Poniżej znajduje się lista oraz krótkie opisy najważniejszych klas w projekcie:

\begin{itemize}
\item \textit{ArgumentParser} --- odpowiedzialna za parsowanie oraz walidację argumentów lini polecań i pliku wejściowego. W razie ewentualnych błędów przerywa program wypisując na ekran komunikat błędu.
\item \textit{Cell} --- klasa reprezentująca komórkę, czyli pojedynczy węzeł siatki. Zawiera m.in. informacje o aktualnym stanie i listę swoich sąsiadów.
\item \textit{Simulation} --- przeprowadza symulację procesu transferu wiedzy. Przechowuje listę obiektów klasy \textit{Cell} (siatkę).  
\item \textit{Statistics} --- klasa odpowiedzialna za moduł statystyk, czyli obliczanych właściwości modelu. Decyduje o tym, które obliczenia numeryczne należy przeprowadzić.    
\item \textit{StatisticsOperation} --- bazowa reprezentacja pojedynczej właściwości modelu. Dziedziczą po niej klasy odpowiedzialne za obliczenia parametrów $n(k), f(k)$ itp.    
\end{itemize} 
\newpage

\section{Plik wejściowy}
\begin{lstlisting}[style=plain, mathescape=true, caption={Format pliku wejściowego.},captionpos=b, label={lst:format},  
breaklines=true,
postbreak=\mbox{\textcolor{red}{$\hookrightarrow$}\space}
]
 $L$*                             : integer > 0
 $K$*                             : double $\in$ $\langle 0,1 \rangle$
 $p$*                             : double $\in$ $\langle 0,1 \rangle$
 $\alpha_a$                              : integer $\in$ $\langle 0,K \rangle$
 $\alpha_b$                              : integer $\in$ $\langle \alpha_a,K \rangle$
 neighborhood                  : 0-Von Neumann,1-Moore,2-Combined
 choice of smartest neighbor   : 0-Random,1-Smartest
 competence to inherit         : 0-Random,1-Clockwise,2-Anticlockwise
 output data                   : f(k),n(k),n(K),tau,f,n(K)vsLp 
 # * - value can be a range, format: [1,2,3,4] = [1:4] = [1:1:4]
\end{lstlisting}

Listing \ref{lst:format} przedstawia akceptowalny format pliku wejściowego. Składa on się z dziewięciu linii. W pierwszych ośmiu definiowany jest jeden z parametrów opisanych w punkcie \ref{cha:model}. Ostatnia linia zawiera wypisane po przecinku charakterystyki modelu, które zostaną obliczone a~ następnie zapisane do plików wyjściowych. Linie rozpoczynające się od $\#$ są ignorowane i traktowane jako komentarz. Program zwraca komunikat błędu, jeżeli plik o~nazwie podanej jako argument wejściowy nie jest zgodny z ustalonym formatem. 

Parametry $L$, $K$ oraz $p$ mogą być zdefiniowane jako proste wyrażenia zamiast wartości skalarnej. Zapis ten możliwy jest na trzy różne sposoby. Pierwszy z nich ma postać [$start : inc : stop$]. Reprezentuje on zbiór liczb rozpoczynający się od $start$, kończący na $stop$, z inkrementem równym $inc$. Drugi sposób definicji jest modyfikacją poprzedniego. Pomijamy w nim parametr $inc$. Jest on równy $1$ w przypadku liczb całkowitych oraz $0.1$ w przypadku liczb zmiennoprzecinkowych. Ostatni zapis jest wypisaniem wszystkich elementów zbioru, np. [$1,2,3,4,5$]. Funkcjonalność ta pozwala zaoszczędzić wiele czasu, ponieważ za jednym uruchomieniem programu możliwe jest skonstruowanie wyników dla dużej liczby konfiguracji parametrów modelu.

\begin{lstlisting}[style=plain, mathescape=true, caption={Przykładowy plik wejściowy.},captionpos=b, label={lst:format1}]
# Plik wejściowy
[5,20]                             #L
[4:8]                              #K		
[0.1:0.2:0.9]                      #p				
1                                  #teaching rate from			
1                                  #teaching rate to			
1                                  #neighborhood				
0                                  #choice of smartest neighbor	
0                                  #way of choosing 
n(k)                               #output data
\end{lstlisting}

Powyższy listing przedstawia plik wejściowy, którego celem jest obliczenie ułamków agentów posiadających $k$ kompetencji. Dzięki zastosowaniu zakresów parametrów uruchomienie aplikacji wygeneruje $2 \times 5 \times 5=50$ plików wyjściowych z danymi. Jest to operacja równoznaczna z pięćdziesięcioma osobnymi wykonaniami programu stosując proste wartości liczbowe. 

\section{Pliki wyjściowe}

Program generuje dwa różne typy plików wyjściowych. Pierwszym z nich są pliki zawierające obliczone dane. Drugi rodzaj to skrypty programu \textit{gnuplot}, które w prosty sposób pozwalają na tworzenie plików graficznych.

\subsection{Pliki z danymi}
Aplikacja generuje osobny plik z danymi dla każdego wybranego parametru zdefiniowanego w ostatnim wierszu pliku wejściowego. Ich nazwy kończą się informacją, jakiego typu dane można znaleźć w środku. Na tę informację składa się nazwa charakterystyki i~dla jakiej konfiguracji modelu została ona wygenerowana (przykładowa nazwa pliku wyjściowego: \textit{dane\_L8K5p05\_n(k).txt}). Pozwala to zachować porządek w katalogach projektu oraz w szybki sposób odnaleźć interesujące nas dokumenty. Niektóre z badanych charakterystyk zestawiane są w zależności od wartości parametru wejściowego modelu. Przykładowo $n(K)$, które oznacza część agentów posiadających wszystkie kompetencje, obliczana i przedstawiana jest w zależności od parametru $p$, czyli początkowego stopnia pokrycia wiedzy. Wartości $p$ należy zdefiniować w pliku wejściowym jako zakres, co jest dodatkowym zastosowaniem zaimplementowanej funkcjonalności określania parametrów modelu jako wyrażenia. Zostanie więc wygenerowany tylko jeden plik o przykładowej nazwie \textit{dane\_L8K5\_n(K).txt} a jego zawartość może prezentować się następująco:

\begin{lstlisting}[style=plain, mathescape=true, caption={Przykładowy plik wyjściowy.},captionpos=b, label={lst:format2}]
#8					#L
#5					#competences		
#[0.2:0.8]	       	                #p				
#1					#teaching rate from			
#1					#teaching rate to			
#1					#neighborhood				
#0					#choice of smartest neighbor	
#0					#way of choosing 
#n(K) 				        #output data
#S=1000
#t	p=0.2	p=0.3	p=0.4	p=0.5	p=0.6	p=0.7	p=0.8
0	0.0625	0.2188	1.0312	3.1562	7.8594	17.0938	32.6875
1	0.0625	0.2188	1.7969	6.9531	20.4844	43.9375	72.0781
2	0.0625	0.3125	3.4062	17.0156	49.5156	79.1875	94.4531
3	0.0625	0.6094	8.4844	42.6250	84.0000	95.8750	98.4375
4	0.0625	1.7188	21.5312	74.5469	97.0625	98.5000	98.7500
\end{lstlisting}

Jak można zauważyć w listingu \ref{lst:format2}, dokumenty z danymi wyjściowymi rozpoczynają się od powtórzonej zawartości pliku wejściowego. Dodatkowo wypisana zostaje wartość o~liczbie przeprowadzonych symulacji, po której uśredniono wyniki. Następnie znajduje się tabela z danymi. Pliki te tworzone są generycznie. Oznacz to, że niezależnie od parametrów symulacji oraz programu, ich wygląd zawsze będzie spójny i intuicyjny.

\subsection{Pliki skryptowe programu gnuplot}

Aplikacja uruchomiona z opcją $-p$ pozwala na generowanie plików skryptowych \textit{gnuplot}. Wywołanie komendy \textit{gnuplot plik.plt} w linii poleceń utworzy plik graficzny z wykresem. Obraz może być zapisany w formacie \textit{png} lub \textit{eps}, w zależności od argumentów wywołania programu. Lista plików \textit{plt} jest identyczna jak lista dokumentów wyjściowych z danymi, z jedyną różnicą jaką jest rozszerzenie plików. 

\begin{figure}[h]
\centering
\includegraphics[scale=0.32]{images/wykres}
\caption{Przykładowy wykres wygenerowany przez stworzony skrypt programu \textit{gnuplot}.}
\label{fig:wykres}
\end{figure}




