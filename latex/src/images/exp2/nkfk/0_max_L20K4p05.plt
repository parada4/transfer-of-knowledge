set terminal postscript eps size 14,10.5 enhanced color font 'Times-Roman,95'
set tics font 'Times-Roman, 70'
set xrange[0:4]
set title '{/Times-Italic L = 20, K = 4, p = 0.5}' offset 0, -0.5 
set xlabel '{/Times-Italic t}' offset 0,1
set key outside
set key font 'Times-Roman, 60'
set key samplen 3 spacing .9
set colorsequence classic
set out '0_max_L20K4p05_n(k).eps'
set yrange[0:100]
set key title '{/Times-Italic k=}'
set ylabel '{/Times-Italic n(k) }[%]' offset 3
set yrange[0:100]
set xtics 1

plot '0_max_L20K4p05.txt' using 1:($2*100) with lp title '0' pointsize 4 lw 4, '0_max_L20K4p05.txt' using 1:($3*100) with lp title '1' pointsize 4 lw 4, '0_max_L20K4p05.txt' using 1:($4*100) with lp title '2' pointsize 4 lw 4, '0_max_L20K4p05.txt' using 1:($5*100) with lp title '3' pointsize 4 lw 4, '0_max_L20K4p05.txt' using 1:($6*100) with lp title '4' pointsize 4 lw 4 
