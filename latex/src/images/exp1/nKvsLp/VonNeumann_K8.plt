set terminal postscript eps size 16,12 enhanced color font 'Times-Roman,95'
set tics font 'Times-Roman, 70'
set title 'K = 8' offset 0,-0.5
set key font 'Times-Roman,60'
set key inside right bottom 
set key title 'L'
set out 'VonNeumann_K8.eps'
set ylabel '{/Times-Italic n(K)} [%]' offset 3
set xlabel '{/Times-Italic p}' offset 0.5
set key samplen 3 spacing .9
set colorsequence classic

set autoscale xy
plot 'VonNeumann_K8.txt' using 1:($2*100) t'5' with linespoints pointsize 6 lw 6 ,'VonNeumann_K8.txt' using 1:($3*100) t'10' with linespoints pointsize 6 lw 6 ,'VonNeumann_K8.txt' using 1:($4*100) t'20' with linespoints pointsize 6 lw 6 ,'VonNeumann_K8.txt' using 1:($5*100) t'50' with linespoints pointsize 6 lw 6 ,'VonNeumann_K8.txt' using 1:($6*100) t'100' with linespoints pointsize 6 lw 6 ,'VonNeumann_K8.txt' using 1:($7*100) t'200' with linespoints pointsize 6 lw 6 ,'VonNeumann_K8.txt' using 1:($8*100) t'500' with linespoints pointsize 6 lw 6 ,'VonNeumann_K8.txt' using 1:($9*100) t'1000' with linespoints pointsize 6 lw 6 
set key off
set yrange[90:100]
set out 'VonNeumann_K8_zoom.eps'
plot 'VonNeumann_K8.txt' using 1:($2*100) t'5' with linespoints pointsize 6 lw 6 ,'VonNeumann_K8.txt' using 1:($3*100) t'10' with linespoints pointsize 6 lw 6 ,'VonNeumann_K8.txt' using 1:($4*100) t'20' with linespoints pointsize 6 lw 6 ,'VonNeumann_K8.txt' using 1:($5*100) t'50' with linespoints pointsize 6 lw 6 ,'VonNeumann_K8.txt' using 1:($6*100) t'100' with linespoints pointsize 6 lw 6 ,'VonNeumann_K8.txt' using 1:($7*100) t'200' with linespoints pointsize 6 lw 6 ,'VonNeumann_K8.txt' using 1:($8*100) t'500' with linespoints pointsize 6 lw 6 ,'VonNeumann_K8.txt' using 1:($9*100) t'1000' with linespoints pointsize 6 lw 6 