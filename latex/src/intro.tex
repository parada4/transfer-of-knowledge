\chapter{Wstęp}
\label{cha:wstep}
\section{Historia automatów komórkowych}

Za twórcę automatów komórkowych uważa się Janosa von Neumanna, wybitnego węgierskiego matematyka, inżyniera, fizyka i informatyka. Jego początkowym zamiarem było stworzenie modelu maszyny samosterującej, która cechowałaby się powielaniem budowy oraz przekazywaniem swoich cech. Jego teoria opierała się na maszynie Turinga, czyli abstrakcyjnym modelu maszyny zdolnej do wykonywania dowolnych operacji na zbiorze elementów w zależności od ich stanów. Pomimo opracowania kilku modeli samoreplikujących się maszyn, ich realizacja okazała się jednak zbyt trudna. Przełom nastąpił, gdy von Neumann wykorzystał pomysł Stanisława Ulama i wprowadził do swojego modelu dyskretny czas oraz przestrzeń. Z tego powodu model automatu komórkowego można określić jako dyskretny odpowiednik równań różniczkowych cząstkowych. Nowy, ulepszony opis samoreprodukującego się modelu automatu komórkowego został przez von Neumanna ostatecznie ukończony w 1952 roku. Komórki umieszczono na siatce dwuwymiarowej, a każda z nich mogła znajdować się w jednym z 29 różnych stanów \cite{Wolfram2002}. Nie doczekał się on jednak nigdy publikacji \cite{CAorigins}. W 1968 roku angielski uczony Edgar Codd w opublikowanej przez Acadamic Press pracy przedstawił rozwinięcie oraz uproszczenie modelu von Neumanna. Pokazał on, że liczba zaproponowanych 29 stanów może być zredukowana do tylko ośmiu.

\vspace{5mm}

Badania te przyczyniły się do powstania najbardziej popularnego automatu komórkowego w historii. Mowa tutaj o stworzonym w 1970 roku przez angielskiego matematyka Johna H. Conwaya automacie \textquote{Gry w życie}. Do jego popularności znacząco przyczynił się fakt, iż był się on tematem rubryki \textquote{Mathematical Games} pisanej przez Martina Gardnera dla amerykańskiego miesięcznika popularnonaukowego Scientific American. Pomimo swojej prostej budowy osiągał on bardzo złożone efekty, co przyczyniło się do wzrostu jego popularności. Z racji tego, że jest on zdolny do każdej operacji logicznej, jest on uznawany za automat uniwersalny \cite{Wolfram2002}.

\vspace{5mm}

Kolejną bardzo ważną osobą w historii jest angielski naukowiec Stephen Wolfram. Pomimo spadku zainteresowaniem automatami komórkowymi w 1983 roku publikuje on pakiet \textquote{Mathematica} oraz dokonuje próby ich klasyfikacji. Dzięki jego działaniom automaty komórkowe odzyskują swoją popularność. Dzięki zdolność opisu ciągłych układów dynamicznych, wolności od błędów zaokrągleń oaz łatwym zrównolegleniu na stałe znajdują swoje zastosowanie w wielu dziedzinach nauki \cite{Wolfram2002}. 

\section{Definicja}
Automat komórkowy jest pojęciem matematycznym. Jest to:
\begin{itemize}
\item sieć $\{i\}$ komórek przestrzeni $D$-wymiarowej,
\item zbiór $\{s_i\}$ stanów pojedynczej komórki,
\item funkcja $F$, która na podstawie stanu komórki oraz stanu jej sąsiadów w chwili $t$, określa jej stan w chwili $t+1$:
\begin{equation} 
 s_i(t+1) = F(\{s_j(t)\}), \text{   gdzie $j$ $\in O(i)$}
\end{equation}
a $O(i)$ oznacza otoczenie komórki. Jeżeli funkcja $F$ jest zależna nie tylko od stanu komórek w poprzedniej chwili czasowej, ale również od zmiennej losowej, to mówimy wtedy o automacie probabilistycznym. \cite{Kulakowski}
\end{itemize}

Innymi słowami automat komórkowy można zdefiniować jako $D$-wymiarową sieć komórek. Każda z komórek w danej chwili czasowej $t$ może znajdować się w dokładnie jednym stanie, a zbiór wszystkich możliwych stanów jest skończony oraz wspólny dla każdej z~nich. Komórki sąsiadujące z daną komórką nazywamy jej otoczeniem lub inaczej jej sąsiadami, których ilość jest z góry ustalona. Bardzo często można spotkać uproszczoną
notację automatów komórkowych, która występuje w postaci dwóch liczb $(k,r)$. Liczba $k$ określa liczbę stanów w jakich może się znaleźć pojedyncza komórka. Liczba $r$ natomiast promień otoczenia. Notacja zwykle wykorzystywana do zapisu automatów jednowymiarowych.


\section{Siatka}
Przestrzeń, w której następuje ewolucja automatu komórkowego nazywamy siatką. Tworzą ją komórki, a każda z nich powinna być wypełnieniem sieci regularnej \cite{Wolfram2002}. Na strukturę siatki wpływają m.in. wymiar przestrzeni, warunek regularności, liczba sąsiadów. 

\begin{itemize}
\item przestrzeń jednowymiarowa --- automat w przestrzeni jednowymiarowej definiowany jest jako lista. Komórki położone są obok siebie, każda z nich sąsiaduje z maksymalnie dwoma innymi komórkami.
\item przestrzeń dwuwymiarowa --- komórki umieszczone są w węzłach sieci dwuwymiarowych. Najczęściej spotykana jest siatka prostokątna, wyróżniamy również m.in. siatkę trójkątną lub heksagonalną. 
\item przestrzeń trójwymiarowa --- komórki automatu zostają umieszczone w węzłach sieci trójwymiarowej.
\end{itemize}

\section{Rodzaje sąsiedztw}
W celu zdefiniowania automatu komórkowego należy wybrać rodzaj sąsiedztwa, czyli otoczenie komórki. Najczęściej używanymi są sąsiedztwo von Neumanna, Moore'a oraz sąsiedztwo złożone (rysunek \ref{fig:sasiedztwa}). Parametrem sąsiedztwa może być promień, który definiuje wielkość otoczenia (rysunek \ref{fig:promien})..

\begin{figure}[h]
\centering
\includegraphics[scale=0.30]{images/sasiedztwa}
\caption{Rodzaje sąsiedztw dla siatki kwadratowej.}
\label{fig:sasiedztwa}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[scale=0.30]{images/promien}
\caption{Sąsiedztwo Moore'a dla $r=0,1,2$.}
\label{fig:promien}
\end{figure}
\newpage

\section{Warunki brzegowe}
Bardzo ważnym elementem w modelu automatu komórkowego są warunki brzegowe. Definiują one jak ma zachowywać się model na brzegach siatki. Są to sytuacje, w której komórka teoretycznie nie posiada wszystkich sąsiadów. Wybór warunków brzegowych zwykle jest ściśle związany z problemem, który automat ma symulować. Najczęściej spotykane warunki brzegowe  to:

\begin{itemize}
\item periodyczne --- zamknięta siatka automatu komórkowego. Sąsiadem komórki leżącej na brzegu siatki jest komórka leżąca po jego przeciwległej stronie. Dla siatki jednowymiarowej powstaje topografia okręgu, natomiast dla siatki dwuwymiarowej torusa (rysunek \ref{fig:torus}).
\item pochłaniające --- brzegi siatki wypełnione są z
góry ustaloną wartością. Warunki brzegowe pochłaniające znajdują zastosowanie w automatach komórkowych, w których samoczynnie generowane są nowe obiekty i wymagane jest ich zredukowanie, by zmniejszyć ich zagęszczenie na siatce automatu. Odpowiada to warunkowi brzegowemu Dirichleta.
\item odbijające --- na krawędziach siatki ustawiana jest bariera, od której symulowane obiekty odbijają się. Wykorzystywane w symulacjach zamkniętych przestrzeni doświadczalnych. 
\end{itemize}

\begin{figure}[h]
\centering
\includegraphics[scale=0.80]{images/torus}
\caption{Odwzorowanie periodycznych warunków brzegowych siatki 2D. Źródło \cite{Torus}.}
\label{fig:torus}
\end{figure}
\newpage


\section{Funkcja przejścia}
Reguły przejść definiują proces ewolucji automatu. Zależą one  od siatki geometrycznej, sąsiedztwa czy przestrzeni stanów. Jeżeli w kolejnych dyskretnych chwilach czasowych stany wszystkich komórek są aktualizowane jednocześnie, mówimy wówczas o automacie synchronicznym. Jeżeli natomiast aktualizacja siatki odbywa się w określonej kolejności, mamy wtedy do czynienie z automatem asynchronicznym. Wyróżniamy trzy główne sposoby definiowania reguł przejść: sposób bezpośredni, sposób wielokrokowy, sposób probabilistyczny.

\section{Warunki początkowe}
Stan symulacji w chwili czasowej $t=0$, czyli stan wszystkich komórek, określają warunki początkowe. Ich konstrukcja zależy głównie od zadanego problemu, często są również generowane losowo. W bardzo znaczący sposób mogą wpływać na przebieg całej symulacji łącznie z jej ostatecznym wynikiem. 

\section{Elementarne automaty komórkowe}
Najprostszymi, a co za tym idzie najlepiej przebadanymi automatami komórkowymi są tzw. jednowymiarowe automaty wolframowskie, czyli automaty klasy $(2, 1)$. Zbiór stanów jest w tym przypadku dwuelementowy - $s_i \in \{0, 1\}$. Rozpatrujemy sąsiedztwo o promieniu równym $r=1$, każda komórka posiada zatem dokładnie dwóch sąsiadów. Elementarnych automatów komórkowych jest $256$, ponieważ na tyle sposobów można określić funkcję przejścia $F$. Gdy spojrzymy na otrzymane wyniki z kolejnych kroków czasowych jak na jeden obraz, możemy zauważyć bardzo interesujące kształty, skonstruowane z pojedynczych komórek. W zależności od swojej klasy mogą tworzyć bardzo różne konfiguracje \cite{Wolfram2002}.
Jedną z bardziej znanych jest reguła \textquote{90}, która po złączeniu stanów symulacji dla kolejnych kroków czasowych generuje fraktal \textquote{trójkąt Sierpińskiego}. Nazwy reguł (od 0 do 255) kodują jednocześnie reguły automatu. Na przykład reguła \textquote{250} opisuje wartości funkcji $F$ dla wszystkich stanów otoczenia w określonej kolejności ($250_{dec}=11111010_{bin}$) - jak pokazano na rysunku \ref{fig:rule250}.

\begin{figure}[h]
\centering
\includegraphics[scale=0.80]{images/rule250}
\caption{Funkcja przejścia reguły \textquote{250}. Źródło \cite{ECA}.}
\label{fig:rule250}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[scale=0.50]{images/rule90}
\caption{Reguła \textquote{90}. Funkcja przejścia oraz jej rezultat. Źródło \cite{Rule90}.}
\label{fig:rule90}
\end{figure}


\section{Gra w życie}
Gra w życie została opracowana przez matematyka Johna Conway’a pod koniec lat 60
XX wieku. Conway początkowo pracował nad stworzeniem matematycznego modelu opisującego zachowanie się organizmów żywych. W tym celu do obliczeń wprowadził synchroniczny automat komórkowy. Komórki rozmieszczone na kwadratowej siatce dwuwymiarowej mogły znajdować się w jednym z dwóch stanów. Z racji na biologiczny charakter pracy komórka mogła być w danej chwili \textquote{żywa} lub \textquote{martwa}. W automacie obowiązywało otoczenie Moore'a oraz następujące reguły przejść:

\begin{itemize}
\item komórka posiadająca trzech żywych sąsiadów, rodzi się lub podtrzymuje swój stan,
\item żywa komórka posiadająca dwóch żywych sąsiadów podtrzymuje swój stan,
\item żywa komórka posiadająca mniej niż dwóch lub więcej niż trzech żywych sąsiadów umiera.
\end{itemize}

Powstałe w ten sposób struktury możemy podzielić na trzy kategorie:

\begin{itemize}
\item znikające po pewnym czasie,
\item zastygające po pewnym czasie,
\item istniejące w nieskończoność, przy czym się poruszają, oscylują lub przemieszczają.
\end{itemize}

\section{Cel pracy}
W dobie szybkiego rozwoju oraz ciągłych zmian wewnątrz organizacji, wiedza jest jednym z najważniejszych zasobów strategicznych. Jest ona zasobem niematerialnym, definiowana zwykle jako zbiór wiarygodnych informacji oraz umiejętności ich efektywnego wykorzystania. Zasoby wiedzy są przypisane do konkretnych osób w organizacji. Traktowane jako całość, w znaczący sposób wpływają na jej ogólną ocenę na rynku oraz stanowią o przewadze konkurencyjnej  \cite{Chen}. Zdolność do nabywania nowych informacji oraz tworzenia wiedzy świadczy o dużej sile organizacyjnej \cite{Nonaka}. Posiadanie zasobów wiedzy wiąże się jednak z szeregiem zadań operacyjnych. Wiedza jest zasobem bardzo trudnym do zarządzania i kontrolowania. Umiejętność odpowiedniego wykorzystywania posiadanych zasobów wiedzy może skutkować w m.in. podniesieniu innowacyjności lub poprawieniu jakości. Jej rozpowszechnianie w obrębie organizacji, a co za tym idzie powiększanie ogólnego zasobu wiedzy, nazywamy transferem wiedzy.

\vspace{5mm}

Transfer wiedzy jest procesem przekazywania, rozpowszechniania lub wymiany wiedzy pomiędzy dwoma, lub więcej jednostkami. Początkowo dzielenie się wiedzą było traktowane jako przekazywanie informacji. Obecnie jednak równie często traktuje się je również jako dynamiczny proces społeczny, w którym wymiana informacji jest tylko składową \cite{VonKrogh} całego procesu. Transfer wiedzy jest jednym z najważniejszych elementów w procesie ogólno pojętego zarządzania oraz rozwoju organizacji. Problem ten jest jednak bardzo trudny i~ skomplikowany. 

\vspace{5mm}

W procesie przekazywania wiedzy w organizacjach występuje wiele barier, czyli sytuacji, w których nie dochodzi to przekazania wiedzy. Bariery te można podzielić na trzy główne kategorie: bariery społeczne, bariery organizacyjne oraz bariery techniczne \cite{Ziebicki}. Przykładową barierą może być tendencja pracowników do nie dzielenia się swoją wiedzą, lub dzielenia się nią bardzo selektywnie. Kolejnym przykładem bariery jest sytuacja, w~ której jednostka obawia się utraty własnej pozycji w organizacji.

\vspace{5mm}

Proces transferu wiedzy wymaga wspólnego wysiłku obu stron, czyli odbiorców i uczących. By móc ten proces uznać za efektywny ważnymi czynnikami są zdolności uczących do przekazywania wiedzy oraz zdolności odbiorców do jej przyswajania.
Istnieją również okoliczności, które są w stanie usprawnić i ulepszyć ten proces. Są nimi przykładowo występowanie kontaktów międzyludzkich oraz relacji nieformalnych. Mogą one wpłynąć zarówno na efektywność transferu wiedzy jak i na ogólne wyniki organizacji. Nieformalna struktura organizacji bardzo sprzyja powstawaniu takich relacji, co z kolei usprawnia omawiany proces. Dzieje się tak, ponieważ ulepszają one współpracę, zmniejszają rywalizację oraz uefektywniają wymianę informacji \cite{IngramRoberts}. Kolejnym pozytywnie wpływającym czynnikiem może być rodzaj zastosowanego przywództwa. Korzystnym rozwiązaniem jest wybór przywództwa rozproszonego, w przeciwieństwie do przywództwa hierarchicznego. Cechuje się ono tym, że role przywódców nie są z góry ustalone natomiast zmieniają się tak, by były jak najlepiej dopasowane do obecnej sytuacji.


\vspace{5mm}

Celem niniejszej pracy jest symulacja procesu transferu wiedzy w obrębie organizacji przy pomocy zaproponowanego modelu automatu komórkowego. Stworzono również aplikację internetową, której zadaniem jest wizualizacja symulacji w zależności od jej parametrów początkowych oraz generowanie przydatnych wykresów w czasie rzeczywistym. Ma ona na celu pomoc w lepszym zrozumieniu cech modelu. Do generowanie wykresów oraz zbierania wyników dla różnych wariacji automatu wykonano aplikację konsolową. Ostatnim etapem pracy jest opracowanie uzyskanych wyników, po czym wyciągnięcie wniosków.














