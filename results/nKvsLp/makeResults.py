def makeResults(name, k, lRange):
    numbers = []
    for l in lRange:
        filename = name+'_L'+str(l)+'K'+str(k)+'_n(K)vsLp.txt'
        with open(filename) as f:
            lastLine = f.readlines()[-1]
            nums = [float(n) for n in lastLine.split()]
            maxx = max(nums)
            if maxx > 1.0:
                nums = [n / (maxx) for n in nums]
            nums[0] = 0.0
            nums.append(1.0)
            numbers.append(nums)

    delimiter = '\t\t'
    filename = name+'_K'+str(k);
    with open(filename + ".txt", 'w') as f:
        f.write("#p"+delimiter)
        for l in lRange:
            f.write('L='+str(l))
            if l==5: f.write('  ')
            elif l<100: f.write(' ')
            f.write(delimiter)
        f.write('\n')

        for i in range(51):
            f.write("{:1.2f}".format(i / 50.0))
            f.write(delimiter)

            for j in range(len(numbers)):
                f.write("{:1.4f}".format(numbers[j][i]))
                f.write(delimiter)

            f.write('\n')
    with open(filename + ".plt", "w") as f:
        f.write("set terminal postscript eps size 14,10.5 enhanced color font 'Helvetica,40'\n"
                "set title 'K = "+str(k)+"'\n"
                "set key font 'Helvetica,20'\n"
                "set key outside\n"
                "set key title 'L'\n"
                "set out '"+name+"_K"+str(k)+".eps'\n"
                "set ylabel 'n(K)'\n"
                "set xlabel 'p'\n"
                "set autoscale xy\nplot ")
        for i in range(2,len(lRange)+2):
            f.write("'"+filename+".txt' using 1:($"+str(i)+"*100) t'"+str(lRange[i-2])+"' with linespoints")
            if i != len(lRange)+2 - 1:
                f.write(",")
        f.write("\n")
        f.write ("set yrange[90:100]\n")
        f.write ("set out '"+name+"_K"+str(k) + "_zoom.eps'\nplot ")
        for i in range(2,len(lRange)+2):
            f.write("'"+filename+".txt' using 1:($"+str(i)+"*100) t'"+str(lRange[i-2])+"' with linespoints")
            if i != len(lRange)+2 - 1:
                f.write(",")


makeResults('VNR2', 8, [5,10,20,50,100,200,500,1000])