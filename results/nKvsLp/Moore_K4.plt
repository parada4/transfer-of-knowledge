set terminal postscript eps size 14,10.5 enhanced color font 'Helvetica,40'
set title 'K = 4'
set key font 'Helvetica,20'
set key outside
set key title 'L'
set out 'Moore_K4.eps'
set ylabel 'n(K)'
set xlabel 'p'
set autoscale xy
plot 'Moore_K4.txt' using 1:($2*100) t'5' with linespoints,'Moore_K4.txt' using 1:($3*100) t'10' with linespoints,'Moore_K4.txt' using 1:($4*100) t'20' with linespoints,'Moore_K4.txt' using 1:($5*100) t'50' with linespoints,'Moore_K4.txt' using 1:($6*100) t'100' with linespoints,'Moore_K4.txt' using 1:($7*100) t'200' with linespoints,'Moore_K4.txt' using 1:($8*100) t'500' with linespoints,'Moore_K4.txt' using 1:($9*100) t'1000' with linespoints
set yrange[90:100]
set out 'Moore_K4_zoom.eps'
plot 'Moore_K4.txt' using 1:($2*100) t'5' with linespoints,'Moore_K4.txt' using 1:($3*100) t'10' with linespoints,'Moore_K4.txt' using 1:($4*100) t'20' with linespoints,'Moore_K4.txt' using 1:($5*100) t'50' with linespoints,'Moore_K4.txt' using 1:($6*100) t'100' with linespoints,'Moore_K4.txt' using 1:($7*100) t'200' with linespoints,'Moore_K4.txt' using 1:($8*100) t'500' with linespoints,'Moore_K4.txt' using 1:($9*100) t'1000' with linespoints