var margins = 20;
var bitSize = 50;
var spacing = {x:30, y:20};
var cellSize;
var c,ctx;
var borderFactor = 0.05;
var border = 3;
var chartFontSize = 15;
var c1,ctx1;
var c2,ctx2;

var FPS = 30;
var animationTime = 1;

var ARROW_T     = 0;
var ARROW_TR    = 1;
var ARROW_R     = 2;
var ARROW_BR    = 3;
var ARROW_B     = 4;
var ARROW_BL    = 5;
var ARROW_L     = 6;
var ARROW_TL    = 7;

var animations = [];
var arrows = [];
var animationStartTime;
var animationEndTime;
var animationStep = 0;
var animationMaxSteps = 0;
var animationThreadId = -1;

function update(competences, p, width, height) {
    bitSize = Math.min(screenWidth / (competences * width + width + 1 - borderFactor * (competences - 1) * width),
                       screenHeight / (2 * height + 1) );
    
    border = borderFactor * bitSize;
    margins = bitSize;
    spacing.x = bitSize;
    spacing.y = bitSize;

    animationThreadId = -1;

    cellSize = competences * bitSize - border * (competences - 1);
    c = document.getElementById("canvas");
    c2 = document.getElementById("canvas2");

    c.width = 2 * margins + spacing.x * (width - 1) + width * cellSize;
    c.height = 2 * margins + spacing.y * (height - 1) + height * bitSize;

    c2.width = 2 * margins + spacing.x * (width - 1) + width * cellSize;
    c2.height = 2 * margins + spacing.y * (height - 1) + height * bitSize;

    ctx = c.getContext("2d");
    ctx.clearRect(0, 0, c.width, c.height);
    ctx.fillStyle = "#FFFFFF";
    ctx.fillRect(0, 0, c.width, c.height);


    ctx2 = c2.getContext("2d");
    ctx2.clearRect(0, 0, c.width, c.height);
    ctx2.fillStyle = "#FFFFFF";
    ctx2.fillRect(0, 0, c.width, c.height);

    c1 = document.getElementById("canvas1");
    ctx1 = c1.getContext("2d");
    c1.width = competences * 14 - (14*border)/bitSize * (competences - 1);
    c1.height = 14;
    ctx1.clearRect(0, 0, c1.width, c1.height);
    ctx1.fillStyle = "#FFFFFF";
    ctx1.fillRect(0, 0, c1.width, c1.height);

    var font = bitSize * 0.7 + "px Arial";
    ctx.font = font;
    ctx.fillStyle = "black";
    ctx.textAlign = "center";
    ctx2.font = font;
    ctx2.fillStyle = "black";
    ctx2.textAlign = "center";
    ctx.fillText("t", c.width/2, bitSize*0.7);
    ctx2.fillText("t-1", c2.width/2, bitSize*0.7);


    updateModel();
    draw();
    drawOR();
    createCharts();

    var pos = {x:margins, y:margins};
    for (var i = 1; i < settings.height - 1; ++i)
    {
        pos.x = margins;
        for (var j = 0; j < settings.width; ++j)
        {
            cells[i][j].setPosition(pos.x, pos.y);
            if (i == 1)
               cells[settings.height - 1][j].setPosition(pos.x, pos.y);
            else if (i == settings.height-2)
               cells[0][j].setPosition(pos.x, pos.y);
            pos.x += cellSize + spacing.x;
        }
        pos.y += bitSize + spacing.y;
    }

}

function draw() {
    var pos = {x:margins, y:margins};
    for (var i = 1; i < settings.height - 1; ++i)
    {
        pos.x = margins;
        for (var j = 0; j < settings.width; ++j)
        {
            drawCell(pos, cells[i][j], ctx);
            drawCell(pos, cells1[i][j], ctx2);
            pos.x += cellSize + spacing.x;
        }
        pos.y += bitSize + spacing.y;
    }
    if (animationThreadId == -1)
    {
        animationThreadId = 0;
    }
    else
    {
        animationStartTime = new Date();
        animationEndTime = new Date();
        animationEndTime.setSeconds(animationStartTime.getSeconds() + 2);
        animationStep = 0;
        animationMaxSteps = FPS * animationTime;
        animationThreadId = setInterval(drawAnimations, 1000 / FPS);
        drawArrows();
    }
}

function drawArrows() {
    ctx.beginPath();
    for (var i = 0; i < arrows.length; ++i)
    {
        var xx = arrows[i].x + arrows[i].competence * (bitSize-border) + border;
        var yy = arrows[i].y + border;
        var fromX, fromY, toX, toY;
        var dir = arrows[i].dir;
        if (dir == ARROW_B)
        {
            fromX = toX = xx + (bitSize - 2 * border) / 2;
            fromY = yy;
            toY = yy + (bitSize - 2 * border);
        }
        else if (dir == ARROW_T)
        {
            fromX = toX = xx + (bitSize - 2 * border) / 2;
            fromY = yy + (bitSize - 2 * border);
            toY = yy;
        }
        else if (dir == ARROW_L)
        {
            fromY = toY = yy + (bitSize - 2 * border) / 2;
            fromX = xx + (bitSize - 2 * border);
            toX = xx;
        }
        else if (dir == ARROW_R)
        {
            fromY = toY = yy + (bitSize - 2 * border) / 2;
            fromX = xx;
            toX = xx + (bitSize - 2 * border);
        }
        else if (dir == ARROW_TL)
        {
            fromX = xx + (bitSize - 2 * border) / 2;
            fromY = yy + (bitSize - 2 * border) / 2;

            toX = xx - (bitSize - 2 * border) / 2;
            toY = yy - (bitSize - 2 * border) / 2;
        }
        else if (dir == ARROW_TR)
        {
            fromX = xx - (bitSize - 2 * border) / 2;
            fromY = yy + (bitSize - 2 * border) / 2;

            toX = xx + (bitSize - 2 * border) / 2;
            toY = yy - (bitSize - 2 * border) / 2;
        }
        else if (dir == ARROW_BL)
        {
            fromX = xx + (bitSize - 2 * border) / 2;
            fromY = yy + (bitSize - 2 * border) / 2;

            toX = xx - (bitSize - 2 * border) / 2;
            toY = yy - (bitSize - 2 * border) / 2;
        }
        else if (dir == ARROW_BR)
        {
            fromX = xx + (bitSize - 2 * border) / 2;
            fromY = yy - (bitSize - 2 * border) / 2;

            toX = xx - (bitSize - 2 * border) / 2;
            toY = yy + (bitSize - 2 * border) / 2;
        }
        if (dir%2==1)
        {
            fromX += (bitSize - 2 * border) / 2;
            fromY += (bitSize - 2 * border) / 2;
            toX += (bitSize - 2 * border) / 2;
            toY += (bitSize - 2 * border) / 2;
        }
        drawArrow(ctx, {x:fromX, y:fromY}, {x:toX, y:toY});
    }
    ctx.strokeStyle = "rgb(0,155,0)";
    ctx.stroke();
}

function drawArrow(ctx, p1, p2){
    var len = bitSize * 0.15;
    var angle = Math.atan2(p2.y-p1.y,p2.x-p1.x);
    ctx.moveTo(p1.x, p1.y);
    ctx.lineTo(p2.x, p2.y);
    ctx.lineTo(p2.x-len*Math.cos(angle-Math.PI/6),p2.y-len*Math.sin(angle-Math.PI/6));
    ctx.moveTo(p2.x, p2.y);
    ctx.lineTo(p2.x-len*Math.cos(angle+Math.PI/6),p2.y-len*Math.sin(angle+Math.PI/6));
}

function drawAnimations()
{
    animationStep += 1;
    var t = animationStep / animationMaxSteps;
    var g = lerp(255, 125, t);
    for (var i = 0; i < animations.length; ++i)
    {
        var x = animations[i].x + animations[i].competence * (bitSize-border) + border;
        var y = animations[i].y + border;
        var size = t * (bitSize - 2 * border);
        ctx.fillStyle = "rgb(0,"+g+",0)";
        ctx.fillRect(x + (1 - t) * (bitSize - 2 * border) / 2, y + (1 - t) * (bitSize - 2 * border) / 2, size, size);
    }

    if (animationStep == animationTime * FPS)
        clearInterval(animationThreadId);
}

function drawCell(pos, cell, pCtx) {
    pCtx.fillStyle = cell.isSmartest() ? "#00FF00" : "#CCCCCC";
    pCtx.fillRect(pos.x + (cell.isSmartest() ? -2 : 0), pos.y + (cell.isSmartest() ? -2 : 0), cellSize  + (cell.isSmartest() ? 4 : 0), bitSize + (cell.isSmartest() ? 4 : 0));
    for (var i = 0; i < settings.competences; ++i)
    {
        pCtx.fillStyle = cell.hasCompetence(i) && !cell.isAnimated(i) ? "#000000" : "#FFFFFF";
        pCtx.fillRect(pos.x + i * (bitSize-border) + border, pos.y + border, (bitSize-2*border), (bitSize-2*border));
    }
}

function drawOR() {
    var oldBitSize = bitSize;
    var oldBorder = border;
    bitSize = 14;
    border = (14*oldBorder)/oldBitSize;
    ctx1.fillStyle = "#CCCCCC";
    ctx1.fillRect(0, 0, cellSize, bitSize);
    ctx1.fillStyle = "#FFFFFF";
    for (var i = 0; i < settings.competences; ++i)
    {
        ctx1.fillStyle = globalOr[i] ? "#000000" : "#FFFFFF";
        ctx1.fillRect(i * (bitSize-border) + border, border, (bitSize-2*border), (bitSize-2*border));
    }
    bitSize = oldBitSize;
    border = oldBorder ;
}

function createCharts() {

    $("#trCanvas").css('height', screenHeight);
    $("#trCanvas2").css('height', screenHeight);

    chart = new CanvasJS.Chart("chartContainer",
        {
            height: screenHeight,
            axisY :{
                labelFontSize : chartFontSize,
                titleFontSize : chartFontSize,
                includeZero: false,
                title: "f(k)",
                titleFontStyle: "italic"
              },
            axisX: {
                labelFontSize : chartFontSize,
                titleFontSize : chartFontSize,
                includeZero: false,
                interval: 1,
                minimum: 0,
                title: "t"
            },
            legend: {
                fontSize : chartFontSize/1.5
            },
            data: chartData
        });

    chart1 = new CanvasJS.Chart("chartContainer1",
        {
            height: screenHeight,
            axisY :{
                labelFontSize : chartFontSize,
                titleFontSize : chartFontSize,
                includeZero: false,
                title: "n(k)",
                titleFontStyle: "italic"
            },
            axisX: {
                labelFontSize : chartFontSize,
                titleFontSize : chartFontSize,
                includeZero: false,
                interval: 1,
                minimum: 0,
                title: "t"
            },
            legend: {
                fontSize : chartFontSize/1.5
            },
            data: chartData1
        });

    chart.render();
    chart1.render();
}

function lerp(from, to, t) {
    return from + (to - from) * t;
}