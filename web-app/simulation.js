var simulationId;
var simulation = false;
var steps = 0;
var chart;
var chart1;
var screenWidth;
var screenHeight;

function loadCookies() {
    var cookie = $.cookie("competences");
    if (typeof cookie != "undefined")
    {
        $("#competences").val(cookie);
        settings.competences = parseInt(cookie);
    }
    cookie = $.cookie("p");
    if (typeof cookie != "undefined")
    {
        $("#p").val(cookie);
        settings.p = parseFloat(cookie);
    }
    cookie = $.cookie("dt");
    if (typeof cookie != "undefined")
        $("#dt").val(cookie);
    cookie = $.cookie("L");
    if (typeof cookie != "undefined")
    {
        $("#L").val(cookie);
        settings.width = parseInt(cookie);
        settings.height = parseInt(cookie) + 2;
    }
    else
    {
        settings.width = 5;
        settings.height = 7;
    }
    cookie = $.cookie("neighbor");
    if (typeof cookie != "undefined")
    {
        $("#neighbor").val(cookie);
        settings.clockwiseNeighbor = parseInt(cookie);
    }
    cookie = $.cookie("inheritance");
    if (typeof cookie != "undefined")
    {
        $("#inheritance").val(cookie);
        settings.inheritanceMethod = parseInt(cookie);
    }
    cookie = $.cookie("neighborhood");
    if (typeof cookie != "undefined")
    {
        $("#neighborhood").val(cookie);
        settings.neighborhood = parseInt(cookie);
    }
}

window.onload = function() {
    loadCookies();
    screenWidth = document.getElementById("tdCanvas").getBoundingClientRect().width;
    screenHeight = document.getElementById("tdCanvas").getBoundingClientRect().height / 2;
    updateSettings();
    updateTeachRange();
};

function updateSettings() {
    simulation = false;
    steps = 0;
    $("#buttonStart").html("Start");
    $("#buttonStart").attr("disabled", false);
    $("#buttonOneStep").attr("disabled", false);
    update(parseInt($("#competences").val()), parseFloat($("#p").val()), parseInt($("#L").val()), parseInt($("#L").val()));
}

function onCompetencesChange() {
    $("#competences").val(minmax($("#competences").val(), 1, 15));
    settings.competences = parseInt($("#competences").val());
    updateSettings();
    $.cookie("competences", $("#competences").val().toString());
    updateTeachRange();
}

function onPChange() {
    $("#p").val(minmax($("#p").val(), 0, 1));
    settings.p = parseFloat($("#p").val());
    updateSettings();
    $.cookie("p", $("#p").val().toString());
}

function onDtChange() {
    $("#dt").val(minmaxFloat($("#dt").val(), 0.01, 100));
    $.cookie("dt", $("#dt").val().toString());
}

function onLChange() {
    $("#L").val(minmax($("#L").val(), 0, 20));
    settings.width = parseInt($("#L").val());
    settings.height = parseInt($("#L").val()) + 2;
    updateSettings();
    $.cookie("L", $("#L").val().toString());
}

function updateTeachRange() {
    $("#teachFrom").html("");
    $("#teachTo").html("");
    for (var i = 0; i < settings.competences+1; ++i)
    {
        $("#teachFrom").html($("#teachFrom").html() + "<option value='"+i+"'>"+i+"</option>")
        $("#teachTo").html($("#teachTo").html() + "<option value='"+i+"'>"+i+"</option>")
    }
    $("#teachFrom").val(0);
    $("#teachTo").val(1);
    onTeachFromChange();
    onTeachToChange();
}

function onStart() {

    if (steps == 0)
    {
        $(".disable").each(function(i) {
            $(this).attr("disabled", true);
        });
    }

    var button = document.getElementById("buttonStart");
    simulation = !simulation;
    button.innerHTML = simulation ? "Stop" : "Start";
    if (simulation)
    {
        var dt = parseFloat($("#dt").val()) * 1000;
        animationTime = dt / 1000;
        simulationId = setInterval(step, dt);
    }
    else
    {
        clearInterval(simulationId);
    }
}
function onOneStep() {

    if (steps == 0)
    {
        $(".disable").each(function(i) {
            $(this).attr("disabled", true);
        });
    }
    animationTime = 1;
    simulation = true;
    step();
    simulation = false;
}

function onReset() {
    resetSimulation();
    draw();
}

function step() {
    doStep();
    if (simulation)
        draw();

    ++steps;
}

function onTeachFromChange() {
    var val = parseInt($("#teachFrom").val());
    if (val > settings.learnTo)
    {
        val = settings.learnTo;
        $("#teachFrom").val(settings.learnTo);
    }
    updateTeachFrom(val);
}

function onTeachToChange() {
    var val = parseInt($("#teachTo").val());
    if (val < settings.learnFrom)
    {
        val = settings.learnFrom;
        $("#teachTo").val(settings.learnFrom);
    }
    updateTeachTo(val);
}

function onNeighborhoodChange() {
    updateNeighborhood(parseInt($("#neighborhood").val()));
    $.cookie("neighborhood", $("#neighborhood").val().toString());
}

function onNeighborChange() {
    updateNeighbor(parseInt($("#neighbor").val()));
    $.cookie("neighbor", $("#neighbor").val().toString());
}

function onInheritanceChange() {
    updateInheritance(parseInt($("#inheritance").val()));
    $.cookie("inheritance", $("#inheritance").val().toString());
}


function minmax(value, min, max)
{
    if(parseInt(value) < min || isNaN(value))
        return min;
    else if(parseInt(value) > max)
        return max;
    else return value;
}

function minmaxFloat(value, min, max)
{
    if(parseFloat(value) < min || isNaN(value))
        return min;
    else if(parseFloat((value) > max))
        return max;
    else return value;
}

function finishSimulation() {
    simulation = false;
    clearInterval(simulationId);
    $("#buttonStart").html("Start");
    $("#buttonStart").attr("disabled", true);
    $("#buttonOneStep").attr("disabled", true);
    $(".disable").each(function(i) {
        $(this).attr("disabled", false);
    });
}

function resetSimulation() {
    $("#buttonStart").html("Start");
    $("#buttonStart").attr("disabled", false);
    $("#buttonOneStep").attr("disabled", false);
    $(".disable").each(function(i) {
        $(this).attr("disabled", false);
    });
    steps = 0;
    simulation = false;
    chartData = [];
    clearInterval(simulationId);
    resetModel();
}

function onGenerete() {
    resetSimulation();
    updateSettings();   
}