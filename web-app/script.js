var rules = {}
var colors = {0 : "#FAF", 1 : "#AFF", 2 : "#AAF"}

var redraw, g, renderer;

var simulation = false;
var simulationId;

var width = 20;
var maxWidth = 0;
var steps = 0;
var numSteps = 0;
var normalStep = [];
var tempStep = [];
var disorderStep = [];

var rows = [];

window.onload = function() {
	createRules();
	prepareStep()
	width = maxWidth = document.getElementById("L").value = parseInt($("#simRow").children().eq(1).outerWidth()/8.7);
 	onWidthBlur();   

	drawGraph();
}

function prepareStep () {
	var row = document.getElementById("simRow");
	row.style.lineHeight = "50%";
	var cell0 = row.insertCell(0);
	var cell1 = row.insertCell(1);
	var cell2 = row.insertCell(2);
	cell0.style.border = "0";
	cell1.style.border = "0";
	cell2.style.border = "0";
	cell1.style.borderRight = "thin dotted #000";
	var el0 = document.createElement("div");
	el0.setAttribute("id", "t");
	cell0.appendChild(el0); 
	var el = document.createElement("div");
	el.setAttribute("id", "normal");
	var el1 = document.createElement("div");
	el1.setAttribute("id", "dist");
	cell1.appendChild(el);
	cell2.appendChild(el1);
	addFirstStep();
}

function addFirstStep () {
	var normal = document.getElementById("normal");
	var dist = document.getElementById("dist");
	var t = document.getElementById("t");
	width = document.getElementById("L").value;
	for (var i = 0; i < width; ++i) {
		var rnd = Math.floor(Math.random() * 3);
		if (i == Math.floor(width/2))
			rnd = 0;
		normal.innerHTML += '<p class="colorBg unselectable pcell pcell1" id="n'+i.toString()+'" onclick="onCellClick(&quot;n'+i.toString()+'&quot;)" style="display:inline; background-color: '+colors[rnd]+';">'+rnd.toString()+'</p>';
		dist.innerHTML += '<p class="colorBg unselectable pcell pcell1" id="d'+i.toString()+'" onclick="onDistCellClick(&quot;d'+i.toString()+'&quot;)" style="display:inline; background-color: '+colors[rnd]+';">'+rnd.toString()+'</p>';
		normalStep.push(rnd);
		disorderStep.push(rnd);
	}
	t.innerHTML = '<p class="colorBg unselectable pcell t" style="display:inline">t=' + steps.toString()+'</p>';
	normal.innerHTML += "<br/>";
	dist.innerHTML += "<br/>";
}

function updateStep () {
	var t = document.getElementById("t" + steps.toString());
	t.innerHTML += '<p class="unselectable pcell" style="display:inline">t=' + (steps+1).toString()+'</p>';
	$(document.getElementById("simTable").rows[3+steps]).show();
	updateSteps(true);
	updateSteps(false);
	if (++steps == numSteps) {
		var button = document.getElementById("buttonStart");
		simulation = !simulation;
		button.innerHTML = "Reset data ->";
		button.disabled = true;

		var rate = document.getElementById("rate");
		rate.innerHTML = 'Disturbance rate: ' + (distCnt / (numSteps * width)).toString();
		clearInterval(simulationId);
	}
}

function updateSteps(normal) {
	var cell = document.getElementById((normal?"normal":"dist") + steps.toString());
	var html = "";
	var array = normal?normalStep:disorderStep;
	for (var i = 0; i < array.length; ++i) {
		var rule = (i == 0 ? array[array.length - 1] : array[i-1]).toString() + array[i].toString() + (i == array.length -1 ? array[0] : array[i+1]).toString();
		var rnd = rules[rule];
		var color = colors[rules[rule]];
		if (!normal && rnd != normalStep[i]) {
			color = "#F00";
			distCnt++;
		} 
		html += '<p class="colorBg unselectable pcell" style="display:inline; background-color: '+color+';">'+rules[rule]+'</p>';
		tempStep[i] = rules[rule];
	}
	for (var i = 0; i < array.length; ++i) {
		array[i] = tempStep[i];
	}
	cell.innerHTML =html;
}

function onCellClick (id) {
	var property = document.getElementById(id);
	var index = id.substring(1, id.size);
	property.innerHTML = id[0] == 'n' ? ++property.innerHTML%3 : normalStep[index];
	var value = parseInt(property.innerHTML);
	property.style.backgroundColor = colors[property.innerHTML];
	if (id[0] == 'n') {
		normalStep[index] = value;
		onCellClick("d" + id.substring(1, id.size));
	} else {
		disorderStep[index] = value;
	}
}

function onDistCellClick (id) {
	var property = document.getElementById(id);
	property.innerHTML = ++property.innerHTML%3;
	var value = parseInt(property.innerHTML);
	property.style.backgroundColor = colors[property.innerHTML];
	var index = id.substring(1, id.size);
	disorderStep[index] = value;
	if (normalStep[index] != disorderStep[index]) {
		property.style.backgroundColor = "#F00";
		$("#"+id).removeClass("colorBg");
		distCnt++;
	} else {
		property.style.backgroundColor = colors[parseInt($("#"+id).html())];
		$("#"+id).addClass("colorBg");
		distCnt--;
	}
}
var distCnt = 0;
var simEver = false;

function onStart () {
	var button = document.getElementById("buttonStart");
	simulation = !simulation;
	button.innerHTML = simulation ? "Stop" : "Start";
	var dt = document.getElementById("dt").value;
	if (!simEver) {
		simEver = true;
		numSteps = document.getElementById("steps").value;;
		var simTable = document.getElementById("simTable");
		rows = new Array(steps);
		for (var i = 0; i < parseInt(numSteps)+1; ++i) {
			var row = simTable.insertRow(3+i);
			if (i != numSteps) {
				row.style.backgroundColor = "#FFF";
				row.style.lineHeight = "5%";
				var cell0 = row.insertCell(0);
				var cell1 = row.insertCell(1);
				var cell2 = row.insertCell(2);
				cell0.style.border = "0";
				cell1.style.border = "0";
				cell2.style.border = "0";
				cell1.style.borderRight = "thin dotted #000";
				var el0 = document.createElement("div");
				el0.setAttribute("id", "t" + i.toString());
				cell0.appendChild(el0); 
				var el = document.createElement("div");
				el.setAttribute("id", "normal" + i.toString());
				var el1 = document.createElement("div");
				el1.setAttribute("id", "dist" + i.toString());
				cell1.appendChild(el);
				cell2.appendChild(el1);
			}
			else {
				row.style.lineHeight = "5%";
				var cell0 = row.insertCell(0);
				cell0.style.border = "0";
				var el0 = document.createElement("div");
				el0.setAttribute("id", "rate");
				cell0.setAttribute("align", "center");
				cell0.setAttribute("colspan", "4");
				cell0.appendChild(el0); 
			}

			$(row).hide();
		}
	}
	if (simulation) {
		tempStep = new Array(width);
		simulationId = setInterval(updateStep, dt * 1000);
		var elems = document.getElementsByClassName("disable");
		for (var i = 0; i < elems.length; ++i)
			elems[i].disabled = true;

		var elems = document.getElementsByClassName("pcell1");
		for (var i = 0; i < elems.length; ++i) {
			elems[i].removeAttribute("onclick");
			elems[i].style.color = "#535353";
		}
	}
	else
		clearInterval(simulationId);
	if (!hideCanvas)
		onHide();
}

function onReset () {
	var table = document.getElementById("rulesTable");
	var trs = table.getElementsByTagName("tr");
	for (var i = trs.length-1; i > 0; --i)
		trs[i].parentElement.removeChild(trs[i]);
	createRules();
	redrawGraph();
	table = document.getElementById("simTable");
	for (var i = 0; i < parseInt(numSteps)+ (simEver?1:0); ++i) {
		$(table.rows[3]).show();
		table.deleteRow(3);
	}
	simEver = false;
	table = document.getElementById("simRow");
	table.innerHTML = "";
	dt = document.getElementById("dt").value = "0.01";
	width = document.getElementById("L").value = maxWidth;
	numSteps = steps = 0;
	document.getElementById("steps").value = "50";
	distCnt = 0;

	// tds = table.getElementsByTagName("td");
	// for (var i = trs.length-1; i > 0; --i)
	// 	trs[i].parentElement.removeChild(trs[i]);
	normalStep = [];
	disorderStep = [];
	prepareStep();

	var bStart = document.getElementById("buttonStart");
	bStart.disabled = false;
	bStart.innerHTML = "Start";
	simulation = false;

	var elems = document.getElementsByClassName("disable");
	for (var i = 0; i < elems.length; ++i)
		elems[i].disabled = false;

	clearInterval(simulationId);
	onWidthBlur();
}

function onWidthBlur () {
	width = document.getElementById("L").value;
	if (width%2==0)width--;
	if (width < 3)
		width = document.getElementById("L").value = 3;
	else if (width > $("#simRow").children().eq(1).outerWidth()/8.7) {
		width = maxWidth;
		if (width%2==0)width--;
	}
	document.getElementById("L").value = width;
	table = document.getElementById("simRow");
	table.innerHTML = "";
	// tds = table.getElementsByTagName("td");w
	// for (var i = trs.length-1; i > 0; --i)
	// 	trs[i].parentElement.removeChild(trs[i]);
	normalStep = [];
	disorderStep = [];
	prepareStep();
}

function createRules () {
	var table = document.getElementById("rulesTable");
	var row1 = table.insertRow(1);
	var row2 = table.insertRow(2);
	var cnt = 0;
	for (var x = 0; x < 3; ++x)
		for (var y = 0; y < 3; ++y)
			for (var z = 0; z < 3; ++z) {
				var rule = x.toString() + y.toString() + z.toString();
				var value = Math.floor((Math.random() * 3));
				rules[rule] = value;
				var titleCell = row1.insertCell(cnt, 2);
				for (var i = 0; i < 3; ++i)
					titleCell.innerHTML += '<p class="colorBg" style="display:inline; background-color:'+colors[parseInt(rule[i])]+'">' + rule[i] +"</p>";	
				var buttonCell = row2.insertCell(cnt++, 2);
				var button = createEmptyButton(buttonCell, rule);
				updateButtonText(button, rule);
				updateButtonColor(button, rule);
				button.setAttribute('onclick', 'onRuleClick("' + rule + '")');
				button.setAttribute("class", "colorBg disable");
			}
}

function createEmptyButton (parent, id) {
	var b = document.createElement("button");
	b.setAttribute("id", id);
	parent.appendChild(b);
	return b;
}

function onRuleClick (id) {
    var property = document.getElementById(id);
    getNextNumber(property, id);
    updateButtonText(property, id);
    updateButtonColor(property, id);

    redrawGraph();
}

function getNextNumber (property, id) {
	var currentNumber = parseInt(property.innerHTML);
	rules[id] = ++currentNumber % 3;	
}

function updateButtonText (property, id) {
    property.innerHTML = rules[id].toString();
}

function updateButtonColor (property, id) {
    property.style.backgroundColor = colors[rules[id]];
}

function redrawGraph () {
	var c = document.getElementById("myCanvas");
	var ctx = c.getContext("2d");
	ctx.clearRect(0, 0, c.width, c.height);
	drawGraph();
}

function drawGraph () {
	var c = document.getElementById("myCanvas");
	var ctx = c.getContext("2d");

	ctx.font = "Bold "+ (15*scale).toString()+"px Arial";
	var startX = 300 - offset.x;
	var startY = 200 - offset.y;    

	var length = 130;
	var angle = -Math.PI/2;
	var angle_stepsize = Math.PI / 4.5;

	var rules2 = ["00", "01", "02", "10", "11", "12", "20", "21", "22"]
	var cnt = 0;

	drawCircles(ctx, posR);
	drawCircles(ctx, posL);

	ctx.beginPath();
	// 00 -> 00
	ctx.moveTo(posR["00"].x, posR["00"].y);
	ctx.lineWidth = getLineWidth("00", "00");
	ctx.quadraticCurveTo((308- offset.x)*scale,(30- offset.y)*scale, posL["00"].x, posL["00"].y);
	ctx.stroke();
	drawLabel(ctx, "000", posR["00"], posL["00"], 'center', 0.5,-2);

	// 11 -> 11
	ctx.beginPath();
	ctx.moveTo(posR["11"].x, posR["11"].y);
	ctx.lineWidth = getLineWidth("11", "11");
	ctx.quadraticCurveTo((354- offset.x)*scale, (355- offset.y)*scale, posL["11"].x, posL["11"].y);
	ctx.stroke();
	drawLabel(ctx, "111", posR["11"], posL["11"], 'center', 0.5,3);
	// 22 -> 22
	ctx.beginPath();
	ctx.moveTo(posR["22"].x, posR["22"].y);
	ctx.lineWidth = getLineWidth("22", "22");
	ctx.quadraticCurveTo((224- offset.x)*scale, (57- offset.y)*scale, posL["22"].x, posL["22"].y);
	ctx.stroke();
	drawLabel(ctx, "222", posR["22"], posL["22"], 'center', 0.5,-2.4);


	drawLineRule(ctx, "00", "01");
	drawLineRule(ctx, "00", "02", 0.25);
	
	drawLineRule(ctx, "01", "10", 0.08);
	drawLineRule(ctx, "01", "11", 0.66);
	drawLineRule(ctx, "01", "12", 0.53);
	
	drawLineRule(ctx, "02", "20", 0.33);
	drawLineRule(ctx, "02", "21", 0.53);
	drawLineRule(ctx, "02", "22", 0.35);
	
	drawLineRule(ctx, "10", "00", 0.2);
	drawLineRule(ctx, "10", "01", 0.62);
	drawLineRule(ctx, "10", "02", 0.5);
	
	drawLineRule(ctx, "11", "10", 0.5);
	drawLineRule(ctx, "11", "12", 0.42);
	
	drawLineRule(ctx, "12", "20", 0.3);
	drawLineRule(ctx, "12", "21", 0.63);
	drawLineRule(ctx, "12", "22", 0.48);
	
	drawLineRule(ctx, "20", "00", 0.5);
	drawLineRule(ctx, "20", "01", 0.4);
	drawLineRule(ctx, "20", "02", 0.63);
	
	drawLineRule(ctx, "21", "10", 0.4);
	drawLineRule(ctx, "21", "11", 0.74);
	drawLineRule(ctx, "21", "12", 0.72);
	
	drawLineRule(ctx, "22", "20", 0.55);
	drawLineRule(ctx, "22", "21", 0.4);

	ctx.lineWidth = 3;
  	ctx.strokeStyle = 'black';
	var xx = 0;
	var yy = 0;
	while (angle < 3/2*Math.PI)
	{
		xx = length * Math.cos (angle);
		yy = length * Math.sin (angle);
		ctx.strokeText(rules2[cnt]/* + " " + parseInt(startX + xx) + " " + parseInt(startY + yy)*/, (xx + startX)*scale, (yy + startY)*scale);
	    ctx.fillStyle = colors[parseInt(rules2[cnt][0])];
    	ctx.fillText(rules2[cnt][0], (xx + startX)*scale, (yy + startY)*scale);
	    ctx.fillStyle = colors[parseInt(rules2[cnt][1])];
    	ctx.fillText(rules2[cnt][1], (xx + startX + ctx.measureText(rules2[cnt++][1]).width/scale)*scale,(yy + startY)*scale);
    	angle += angle_stepsize;
	}
}
var posR = {"00" : {x:327, y:64}, "01":{x:410, y:95 }, "02":{x:455, y:172}, "10":{x:437, y:262}, "11":{x:372, y:317}, "12":{x:283, y:317}, "20":{x:210, y:260},"21":{x:195, y:172}, "22":{x:242, y:92 }};
var posL = {"00" : {x:290, y:64}, "01":{x:380, y:95 }, "02":{x:425, y:172}, "10":{x:408, y:262}, "11":{x:335, y:317}, "12":{x:250, y:317}, "20":{x:180, y:260},"21":{x:160, y:172}, "22":{x:205, y:92 }};

var posRI = {"00" : {x:327, y:64}, "01":{x:410, y:95 }, "02":{x:455, y:172}, "10":{x:437, y:262}, "11":{x:372, y:317}, "12":{x:283, y:317}, "20":{x:214, y:260},"21":{x:199, y:172}, "22":{x:242, y:92 }};
var posLI = {"00" : {x:290, y:64}, "01":{x:375, y:95 }, "02":{x:420, y:172}, "10":{x:403, y:262}, "11":{x:335, y:317}, "12":{x:250, y:317}, "20":{x:177, y:260},"21":{x:160, y:172}, "22":{x:205, y:92 }};

var labels = false;

var offset = {x:140,y:30};
var scale = 1;
var scaleInit = true;
function drawCircles(ctx, pos) {
	var posI = pos==posL?posLI:posRI;
	for (var k in pos){
	    if (pos.hasOwnProperty(k)) {
	    	if (scaleInit) {
		    	pos[k].x = posI[k].x-offset.x;
		    	pos[k].y = posI[k].y-offset.y;
		    	pos[k].x *= scale;
		    	pos[k].y *= scale;
	    	}
	        ctx.beginPath();
		    ctx.arc(pos[k].x, pos[k].y, 4, 0, 2 * Math.PI, false);
		    ctx.fillStyle = 'rgba(0,0,255,0.8)';
		    ctx.fill();
		    ctx.lineWidth = 1;
		    ctx.stroke();
		}
	}
	if (scaleInit && pos == posL)
		scaleInit = false;
}

function getLineWidth(xy, yz) {
	return (rules[xy+yz[1]]).toString() == xy[1] ? 3 : 1;
}

function drawLineRule(ctx, xy, yz, padding, paddingTop) {
	var p1 = posR[xy];
	var p2 = posL[yz];
	ctx.beginPath();
	ctx.moveTo(p1.x, p1.y);
	ctx.lineWidth = getLineWidth(xy, yz);
	//ctx.lineTo(p2.x, p2.y);
	drawArrow(ctx, p1, p2);
	ctx.stroke();
	drawLabel(ctx, xy+yz[1], p1, p2, 'left', padding?padding:0.5, paddingTop?paddingTop:-0.1, getLineWidth(xy,yz)==3);
}

var shouldArrow = false;

function onArrow() {
	shouldArrow = !shouldArrow;
	redrawGraph();
}

function drawArrow(ctx, p1, p2){
    var len = 15;
    var angle = Math.atan2(p2.y-p1.y,p2.x-p1.x);
    ctx.moveTo(p1.x, p1.y);
    ctx.lineTo(p2.x, p2.y);
    if (shouldArrow) {
	    ctx.lineTo(p2.x-len*Math.cos(angle-Math.PI/6),p2.y-len*Math.sin(angle-Math.PI/6));
	    ctx.moveTo(p2.x, p2.y);
	    ctx.lineTo(p2.x-len*Math.cos(angle+Math.PI/6),p2.y-len*Math.sin(angle+Math.PI/6));
    }
}

function drawLabel( ctx, text, p1, p2, alignment, padding, paddingTop, bold){
   ctx.fillStyle = '#000';
   if (!labels)
  	return;
  if (p2.x < p1.x) {
  	var p3 = p1; p1 = p2; p2 = p3;
  }

  if (!alignment) alignment = 'center';
  if (!padding) padding = 0;
  // alert(padding);
  var dx = p2.x - p1.x;
  var dy = p2.y - p1.y;   
  var p, pad;
  if (alignment=='center'){
    p = p1;
    pad = 1/2;
  } else {
    var left = alignment=='left';
    p = left ? p1 : p2;
    // pad = padding / Math.sqrt(dx*dx+dy*dy) * (left ? 1 : -1);
    pad = padding;
  }

  ctx.save();
  ctx.font =  (bold ? "Bold " : "") + (10*scale).toString()+"px Arial";
  ctx.textAlign = alignment;
  var center = {x:p.x+dx*pad,y:p.y+dy*pad};
  var rot = rotate2DVector({x:dx,y:dy}, Math.PI/2);
  var length = Math.sqrt(rot.x * rot.x + rot.y * rot.y);
  rot.x /= length; rot.x *= 10*scale;
  rot.y /= length; rot.y *= 10*scale;
  ctx.translate(center.x + rot.x*paddingTop, center.y + rot.y*paddingTop);
  ctx.rotate(Math.atan2(dy,dx));
  ctx.fillText(text,0,0);
  ctx.restore();
}

function rotate2DVector(p, angle) {
	var p1 = {x:0,y:0};
	p1.x = p.x * Math.cos(angle) - p.y * Math.sin(angle);
	p1.y = p.x * Math.sin(angle) + p.y * Math.cos(angle);
	return p1;
}

function onLabels() {
	labels = !labels;
	redrawGraph();
}

function onZoom(mult) {
	var newScale = scale + 0.2 * mult;
	if (newScale-0.6 < 0.1) {
		document.getElementById("zm").disabled = true;
		return;
	} else if (Math.abs(newScale-2.2) <0.1) {
		document.getElementById("zp").disabled = true;
		return;
	} 
	document.getElementById("zp").disabled = false;
	document.getElementById("zm").disabled = false;
	
	scale = newScale;
	scaleInit = true;
	var ctx = $("#myCanvas")[0].getContext('2d'); 
	ctx.canvas.width = scale * 340;
	ctx.canvas.height = scale * 330;
    redrawGraph();
}

var hideCanvas = false;

function onHide(){
	hideCanvas = !hideCanvas;
	$("#hide").text(hideCanvas ? "\\/":("/"+'\\'));
	if (hideCanvas) {
		$('#trGraph').hide();
		$('#trGraph1').hide();
	} else {
		$('#trGraph').show();
		$('#trGraph1').show();
	} 
}

function onChangeColor(picker, val) {
	colors[val] = "#"+picker.toString();
	$(".colorBg").each(function(i) {
    	$(this).css('background-color', colors[parseInt($(this).html())]);
    });

    redrawGraph();
}