var VON_NEUMANN = 0;
var MOORE = 1;

var cells = [];

var settings = {
    competences : 4,
    p : 0.5,
    width : 5,
    height : 5,
    clockwiseNeighbor : 0,
    inheritanceMethod : 1,
    learnFrom : 0,
    learnTo : 1,
    neighborhood : 0
};

var chartData = [];
var normalizedValues = [];

var chartData1 = [];
var normalizedValues1 = [];

var initSum = 0;
var totalSum = 0;
var prevSum = 0;

var globalOr;

var cells1 = [];

function updateModel() {
    animations = arrows = [];
    updateArray();
    updateChart();
}

function updateChart() {
    chartData = [];
    for (var i = 0; i < settings.competences; ++i)
    {
        chartData.push(
            {
                type: "line",
                showInLegend: true,
                legendText: "k="+i.toString(),
                dataPoints: []
            });
    }

    normalizedValues = new Array(settings.competences);
    for(i = 0; i < settings.competences; ++i)
        normalizedValues[i] = 0;

    for(i = 1; i < settings.height - 1; ++i)
        for (j = 0; j < settings.width; ++j)
        {
            for (var k = 0; k < settings.competences; ++k)
                if (cells[i][j].hasCompetence(k))
                    normalizedValues[k] += 1;
        }

    for(i = 0; i < settings.competences; ++i)
    {
        normalizedValues[i] /= settings.width * (settings.height-2);
        chartData[i].dataPoints.push({x:steps, y:normalizedValues[i]});
    }

    chartData1 = [];
    for (i = 0; i < settings.competences + 1; ++i)
    {
        chartData1.push(
            {
                type: "line",
                showInLegend: true,
                legendText: "k="+i.toString(),
                dataPoints: []
            });
    }

    normalizedValues1 = new Array(settings.competences + 1);
    for(i = 0; i < settings.competences + 1; ++i)
        normalizedValues1[i] = 0;

    for(i = 1; i < settings.height - 1; ++i)
        for (j = 0; j < settings.width; ++j)
        {
            var sum = 0;
            for (k = 0; k < settings.competences; ++k)
                if (cells[i][j].hasCompetence(k))
                    sum += 1;
            normalizedValues1[sum] += 1;
        }

    for(i = 0; i < settings.competences + 1; ++i)
    {
        normalizedValues1[i] /= settings.width * (settings.height-2);
        chartData1[i].dataPoints.push({x:steps, y:normalizedValues1[i]});
    }

}

function updateArray() {
    cells = new Array(settings.height);
    cells1 = new Array(settings.height);
    globalOr = new Array(settings.competences);
    for(var i = 0; i < settings.competences; ++i)
        globalOr[i] = 0;

    var maxSum = 0;
    var maxCells = [];
    for(var i = 0; i < settings.height; ++i)
    {
        cells[i] = new Array(settings.width);
        cells1[i] = new Array(settings.width);
    }
    for(i = 0; i < settings.height; ++i)
    {
        for(var j = 0; j < settings.width; ++j)
        {
            cells[i][j] = new Cell;
            cells1[i][j] = new Cell;
            cells[i][j].init(settings.competences, settings.p, i, j);
            cells1[i][j].init(settings.competences, settings.p, i, j);
            if (i != 0 && i != settings.height - 1)
            {
                if (cells[i][j].getSum() > maxSum)
                {
                    maxSum = cells[i][j].getSum();
                    maxCells = [cells[i][j]];
                }
                else if (cells[i][j].getSum() == maxSum)
                    maxCells.push(cells[i][j]);

                totalSum += cells[i][j].getSum();

                for (var k = 0; k < settings.competences; ++k)
                    globalOr[k] |= cells[i][j].hasCompetence(k);
            }
        }
    }
    for (i = 0; i < maxCells.length; ++i)
        maxCells[i].setSmartest();

    copyFirstAndLast();
    assignNeighbors();

    initSum = totalSum;

    copyArrayOfCells(cells, cells1);
}

function assignNeighbors() {
    for(var i = 1; i < settings.height - 1; ++i)
        for(var j = 0; j < settings.width; ++j)
            cells[i][j].setNeighbors();
}

function doStep() {
    animationStep = animationMaxSteps-1;
    drawAnimations();
    clearInterval(animationThreadId);
    copyFirstAndLast();
    copyArrayOfCells(cells, cells1);
    prevSum = totalSum;
    totalSum = 0;
    animations = [];
    arrows = [];
    for(var i = 1; i < settings.height - 1; ++i)
        for (var j = 0; j < settings.width; ++j)
            cells[i][j].doStep();

    normalizedValues = new Array(settings.competences);
    normalizedValues1 = new Array(settings.competences + 1);
    for(i = 0; i < settings.competences; ++i)
        normalizedValues[i] = 0;
    for(i = 0; i < settings.competences+1; ++i)
        normalizedValues1[i] = 0;

    for(i = 1; i < settings.height - 1; ++i)
        for (j = 0; j < settings.width; ++j)
            cells[i][j].completeStep();

    for(i = 0; i < settings.competences; ++i)
    {
        normalizedValues[i] /= settings.width * (settings.height-2);
        chartData[i].dataPoints.push({x:steps+1, y:normalizedValues[i]});
    }

    for(i = 0; i < settings.competences+1; ++i)
    {
        normalizedValues1[i] /= settings.width * (settings.height-2);
        chartData1[i].dataPoints.push({x:steps+1, y:normalizedValues1[i]});
    }

    chart.render();
    chart1.render();

    if (totalSum == prevSum)
        finishSimulation();
}

function updateTeachFrom(val) {
    settings.learnFrom = val;
}

function updateTeachTo(val) {
    settings.learnTo = val;
}

function updateNeighborhood(val) {
    settings.neighborhood = val;
    assignNeighbors();
}

function updateNeighbor(val) {
    settings.clockwiseNeighbor = val;
}

function updateInheritance(val) {
    settings.inheritanceMethod = val;
}

function copyFirstAndLast() {
    for(var j = 0; j < settings.width; ++j)
    {
        cells[0][j].copyCell(cells[settings.height-2][j]);
        cells[settings.height-1][j].copyCell(cells[1][j]);
    }

}

function resetModel() {
    for(var i = 1; i < settings.height - 1; ++i)
        for(var j = 0; j < settings.width; ++j)
            cells[i][j].restoreInitValues();

    copyFirstAndLast();
    totalSum = initSum;

    copyArrayOfCells(cells, cells1);

    clearInterval(animationThreadId);
    animationThreadId = -1;
    animations = arrows = [];
    updateChart();
    createCharts();
}

function print() {
    for(var i = 0; i < settings.height; ++i)
    {
        for(var j = 0; j < settings.width; ++j)
        {
            str += cells[i][j].toString() + '\t';
        }
        str += '\n';
    }
    str += '\n';
    alert(str);
}

function printSums(clean) {
    if (clean)
        str = "";
    for(var i = 0; i < settings.height; ++i)
    {
        for(var j = 0; j < settings.width; ++j)
        {
            str += cells[i][j].toStringSum() + '\t';
        }
        str += '\n';
    }
    str += '\n';
    alert(str);
}


function printNeighborSums(clean) {
    if (clean)
        str = "";
    for(var i = 1; i < settings.height-1; ++i)
    {
        for(var j = 0; j < settings.width; ++j)
        {
            str += cells[i][j].toStringNeighborSum() + '\t';
        }
        str += '\n';
    }
    str += '\n';
    alert(str);
}

function printNeighborPos(clean) {
    if (clean)
        str = "";
    for(var i = 1; i < settings.height-1; ++i)
    {
        for(var j = 0; j < settings.width; ++j)
        {
            str += cells[i][j].toStringNeighborPos() + '\t';
        }
        str += '\n';
    }
    str += '\n';
    alert(str);
}

function clone(destination, source) {
    for (var property in source) {
        if (typeof source[property] === "object" && source[property] !== null && destination[property]) {
            clone(destination[property], source[property]);
        } else {
            destination[property] = source[property];
        }
    }
}

function IJtoIndex(i, j) {
    return settings.width * i + j;
}

function indexToIJ(index) {
    if (index < 0)
    {
        index = settings.width*(settings.height-2)+index;
    }
    else if (index >= settings.width*settings.height)
    {
        index = 0;
    }
    return {j:index%settings.width,i:Math.floor(index/settings.width)}
}
