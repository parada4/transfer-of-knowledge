function Cell()
{
    var initValues = {
        data : [],
        sum : 0
    };

    var currArray = [];
    var copyArray = [];
    var sum = 0;
    var copySum = 0;
    var neighbors = [];
    var smartest = false;
    var animated = false;
    var animatedComp = -1;
    var pos = {i:0, j:0};
    var x = 0;
    var y = 0;
    var smarterOne;
    var complete = false;

    this.init = function (competences, p, i, j) {
        sum = 0;
        pos.i = i;
        pos.j = j;
        if (currArray.length != competences)
        {
            currArray = new Array(competences);
            copyArray = new Array(competences);
            initValues.data = new Array(competences);
        }
        for (var i = 0; i < competences; ++i)
        {
            currArray[i] = +(Math.random() > (1-p));
            copyArray[i] = currArray[i];
            initValues.data[i] = currArray[i];
            sum += currArray[i];
        }
        initValues.sum = sum;
    };

    this.hasCompetence = function (i) {
        return currArray[i] == 1;
    };

    this.setNeighbors = function () {
        neighbors = new Array(settings.neighborhood == VON_NEUMANN ? 4 : 8);
        var index = IJtoIndex(pos.i, pos.j);
        if (settings.neighborhood == VON_NEUMANN)
        {
            var n = indexToIJ(index - settings.width);
            neighbors[0] = cells[n.i][n.j];
            n = indexToIJ(index + 1);
            neighbors[1] = cells[n.i][n.j];
            n = indexToIJ(index + settings.width);
            neighbors[2] = cells[n.i][n.j];
            n = indexToIJ(index - 1);
            neighbors[3] = cells[n.i][n.j];
        }
        else if (settings.neighborhood == MOORE)
        {
            n = indexToIJ(index - settings.width);
            neighbors[0] = cells[n.i][n.j];
            n = indexToIJ(index - settings.width + 1);
            neighbors[1] = cells[n.i][n.j];
            n = indexToIJ(index + 1);
            neighbors[2] = cells[n.i][n.j];
            n = indexToIJ(index + settings.width + 1);
            neighbors[3] = cells[n.i][n.j];
            n = indexToIJ(index + settings.width);
            neighbors[4] = cells[n.i][n.j];
            n = indexToIJ(index + settings.width - 1);
            neighbors[5] = cells[n.i][n.j];
            n = indexToIJ(index - 1);
            neighbors[6] = cells[n.i][n.j];
            n = indexToIJ(index - settings.width - 1);
            neighbors[7] = cells[n.i][n.j];
        }
    };

    this.getSum = function () {
        return sum;
    };

    this.doStep = function () {
        animated = false;
        this.copyArray(copyArray, currArray);
        copySum = sum;
        var smarterIndexes = [];
        for (var i = 0; i < neighbors.length; ++i)
            if (neighbors[i].getSum() >= sum + settings.learnFrom && neighbors[i].getSum() <= sum + settings.learnTo)
            {
                if (neighbors[i].getSum() - sum == 0)
                {
                    var theSameCells = true;
                    for (var j = 0; j < settings.competences; ++j)
                    {
                        if (neighbors[i].hasCompetence(j) != this.hasCompetence(j))
                        {
                            theSameCells = false;
                            break;
                        }
                    }
                    if (!theSameCells)
                        smarterIndexes.push(i);
                } else
                    smarterIndexes.push(i);
            }

        if (smarterIndexes.length > 0)
        {
            if (settings.clockwiseNeighbor == 1)
            {
                var maxSum = Math.max.apply(Math,smarterIndexes.map(function(i){return neighbors[i].getSum();}));
                smarterIndexes = smarterIndexes.filter(function(i) { return neighbors[i].getSum() == maxSum;});
                console.log(pos.i + ", " + pos.j + "  [" + smarterIndexes + "]");
             }
            
            smarterOne = neighbors[smarterIndexes[getRandomInt(0, smarterIndexes.length-1)]];

            var competences = [];
            for (i = 0; i < currArray.length; ++i)
                if (smarterOne.hasCompetence(i) && !currArray[i])
                    competences.push(i);

            if (settings.inheritanceMethod == 1)
                copyArray[competences[getRandomInt(0, competences.length-1)]] = 1;
            else if (settings.inheritanceMethod == 2)
                copyArray[competences[0]] = 1;
            else
                copyArray[competences[competences.length - 1]] = 1;

            copySum += 1;
        }
    };

    this.completeStep = function () {
        complete = true;
        this.copyArray(currArray, copyArray);
        complete = false;
        sum = copySum;
        totalSum += sum;
    };

    this.print = function () {
        alert(currArray);
    };

    this.toString = function () {
        return currArray.join('');
    };

    this.toStringSum = function () {
        return sum.toString();
    };

    this.toStringNeighborSum = function () {
        return [neighbors[0].getSum(), neighbors[1].getSum(), neighbors[2].getSum(), neighbors[3].getSum()].join(',');
    };

    this.toStringNeighborPos = function () {
        return [neighbors[0].toStringPos(), neighbors[1].toStringPos(), neighbors[2].toStringPos(), neighbors[3].toStringPos()].join(',');
    };
    this.toStringPos = function () {
        return pos.i.toString() + ' ' + pos.j.toString();
    };

    this.copyCell = function (cell) {
        for (var i = 0; i < currArray.length; ++i)
        {
            currArray[i] = +cell.hasCompetence(i);
            copyArray[i] = +cell.hasCompetence(i);
            sum = cell.getSum();
        }
    };

    this.setPosition = function (posX, posY) {
        x = posX;
        y = posY;
    };

    this.setSmartest = function () {
        smartest = true;
    };

    this.isSmartest = function () {
        return smartest;
    };

    this.isAnimated= function (i) {
        return animated && animatedComp == i;
    };

    this.restoreInitValues = function () {
        for (var i = 0; i < initValues.data.length; ++i)
            copyArray[i] = currArray[i] = initValues.data[i];

        sum = copySum = initValues.sum;
    };

    this.copyArray = function (to, from) {
        var sum = 0;
        for (var i = 0; i < from.length; ++i)
        {
            if (complete)
            {
                if (to[i] != from[i])
                {
                    animations.push({competence:i, x:x, y:y});
                    animated = true;
                    animatedComp = i;
                    if (settings.neighborhood == VON_NEUMANN)
                        var dir =   smarterOne == neighbors[0] ? ARROW_B :
                                    smarterOne == neighbors[1] ? ARROW_L :
                                    smarterOne == neighbors[2] ? ARROW_T :
                                    ARROW_R;
                    else if (settings.neighborhood == MOORE)
                        var dir =   smarterOne == neighbors[0] ? ARROW_B :
                                    smarterOne == neighbors[1] ? ARROW_BR :
                                    smarterOne == neighbors[2] ? ARROW_L :
                                    smarterOne == neighbors[3] ? ARROW_TL :
                                    smarterOne == neighbors[4] ? ARROW_T :
                                    smarterOne == neighbors[5] ? ARROW_TR :
                                    smarterOne == neighbors[6] ? ARROW_R :
                                    ARROW_BL;

                    arrows.push({competence:i, dir:dir, x:smarterOne.getX(), y:smarterOne.getY()});
                }
                normalizedValues[i] += from[i];
                sum += from[i];
            }
            to[i] = from[i];
        }
        if (complete)
            normalizedValues1[sum] += 1;
    };

    this.getX = function () {
        return x;
    };

    this.getY = function () {
        return y;
    };

    this.getPos = function () {
        return pos;
    }
}


function copyArrayOfCells(from, to) {
    for (var i = 0; i < from.length; ++i)
    {
        for (var j = 0; j < from[0].length; ++j)
        {
            to[i][j].copyCell(from[i][j]);
        }
    }
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}